# Das Erste Programm

<!-- TOC -->

- [Das Erste Programm](#das-erste-programm)
  - [Wie Go Programme zu lesen sind](#wie-go-programme-zu-lesen-sind)
  - [Überprüf' Dein Wissen](#%C3%BCberpr%C3%BCf-dein-wissen)

Traditionell wird das erste Programm, das du in einer beliebigen Programmiersprache schreibst, als "Hello World"-Programm bezeichnet - ein Programm, das einfach nur Hello World auf deinem Terminal anzeigt... Laß uns eins mit Go schreiben!

Erstelle zunächst einen neuen Ordner, in dem wir unser Programm speichern können. Erstelle einen Ordner mit dem Namen ~/src/go/src/ag/hello. (Wobei ~ Dein Homeverzeichnis bedeutet) Vom Terminal aus kannst du dies durch Eingabe des folgenden Befehls tun:

```sh
mkdir ~/go/src/ag/hello
```

Erstelle nun mit Visual Studio Code eine neue Datei in dem Ordner, den du gerade erstellt hast, und
nenne sie **main.go**.

Gib' nun folgendes in die Datei ein:

```go
package main

import "fmt"

// Dies ist ein Kommmentar. Es hat keine Auswirkung auf das Programm

func main() {
    fmt.Println("Hello World")
}
```

Achte darauf, dass deine Datei mit der hier gezeigten identisch ist und speichere sie als main.go in dem Ordner, den wir gerade erstellt haben. Öffne ein neues Terminal und gib' Folgendes ein (achte darauf, nach jeder Zeile mit Enter zu bestätigen):

```plain
$ cd ~/go/src/ag/hello
$ go run main go
```

Du solltest `Hello World` in deinem Terminal sehen. Der Befehl `go run` nimmt die nachfolgenden Dateien (getrennt durch Leerzeichen), kompiliert sie zu einer ausführbaren Datei, die in einem temporären Verzeichnis gespeichert wird, und führt dann das Programm aus. Wenn du Hello World nicht angezeigt bekommen hast, hast du vielleicht einen Fehler gemacht, als du das Programm eingegeben hast. Der Go-Compiler gibt dir Hinweise, wo der Fehler liegt. Wie die meisten Compiler ist auch der Go-Compiler äußerst pedantisch und toleriert keine Fehler.

## Wie Go Programme zu lesen sind

Schauen wir uns dieses Programm genauer an. Go-Programme werden von oben nach unten und von links nach rechts gelesen. (wie ein Buch) Die erste Zeile lautet wie folgt:

```go
package main
```

Dies wird als " Package-Deklaration " bezeichnet. Jedes Go-Programm muss mit einer Package-Deklaration beginnen. Pakete sind Go's Methode zur Organisation und Wiederverwendung von Code. Es gibt zwei Arten von Go-Programmen: ausführbare Dateien und (Programm)Bibliotheken. Ausführbare Anwendungen sind die Arten von Programmen, die wir direkt vom Terminal aus ausführen können. (unter Windows enden sie mit .exe) Bibliotheken sind Sammlungen von Code, die wir zusammenpacken, damit wir sie in anderen Programmen verwenden können. Wir werden Bibliotheken später näher betrachten, denn jetzt solltest du nur sicherstellen, dass diese Zeile in jedes Programm eingefügt wird, das du schreibst.

Die nächste Zeile ist eine Leerzeile. Computer repräsentieren Zeilenumbrüche mit einem Sonderzeichen (oder mehreren Zeichen). Newlines, Leerzeichen und Tabs werden als Whitespace bezeichnet (weil man sie nicht sehen kann). Go interessiert sich nicht für Whitespace, er wird bei der Auswertung des Codes ignoriert. Wir benutzen ihn nur, um Programme leichter lesbar zu machen. (Du könntest diese Zeile entfernen und das Programm würde sich genauso verhalten).

Dann sehen wir das hier:

```go
import "fmt"
```

Das ``import``-Keyword ist, wie wir Code aus anderen Packages einbinden, den wir in unserem Programm verwenden. Das fmt-Paket (kurz für Format) implementiert Formatierung für Ein- und Ausgabe. Angesichts dessen, was wir gerade über Pakete gelernt haben, was glaubst du, was in den Dateien des fmt-Pakets oben drin stehen würde? (Tipp: ``package fmt``)

Beachte, dass **fmt** oben von doppelten Anführungszeichen umgeben ist. Die Verwendung solcher doppelten Anführungszeichen ist als *Stringliteral* bekannt, was eine Form von **Ausdruck** ist. In Go stellen **Strings**, zu Deutsch **Zeichenketten**, eine Folge von Zeichen (Buchstaben, Zahlen, Symbole,...) mit einer bestimmten Länge dar. Zeichenketten werden im nächsten Abschnitt näher beschrieben, aber im Moment ist es wichtig zu beachten, dass auf ein sich öffnendes **"** Zeichen schließlich ein anderes **"** Zeichen folgen muss und alles dazwischen in die Zeichenkette aufgenommen wird. (Das Zeichen " selbst ist nicht Teil der Zeichenkette)

Die Zeile, die mit ``//`` beginnt, wird als Kommentar bezeichnet. Kommentare werden vom Go-Compiler ignoriert und sind um deinetwillen da (oder wer auch immer den Quellcode für dein Programm auslesen will). Go unterstützt zwei verschiedene Arten von Kommentaren: ``//``Kommentare, in denen der gesamte Text zwischen dem // und dem Ende der Zeile Teil des Kommentars ist und ``/* */`` Kommentare, in denen alles zwischen den *'s Teil des Kommentars ist. (Kann auch mehrere Zeilen beinhalten)

Danach siehst du eine Funktionsdeklaration:

```go
func main() {
    fmt.Println("Hello World")
}
```

Funktionen sind die Bausteine eines Go-Programms. Sie haben Inputs, Outputs und eine Reihe von Schritten, die als Anweisungen bezeichnet werden und in der Reihenfolge des Auftretens ausgeführt werden. Alle Funktionen beginnen mit dem Schlüsselwort ``func``, gefolgt vom Namen der Funktion (in diesem Fall ``main``), einer Liste von null oder mehr "Parametern", umgeben von Klammern, einem optionalen Rückgabetyp und einem "body", der von geschweiften Klammern umgeben ist. Diese Funktion hat keine Parameter, gibt nichts zurück und hat nur eine Anweisung. Der Name main ist speziell, weil es die Funktion ist, die beim Ausführen des Programms aufgerufen wird.

Das letzte Stück unseres Programms ist diese Zeile:

```go
   fmt.Println("Hello World")
```

Diese Anweisung setzt sich aus drei Komponenten zusammen. Zuerst greifen wir auf eine andere Funktion innerhalb des fmt-Pakets namens Println zu (das ist das Stück ``fmt.Println``, Println bedeutet "Print Line"). Dann erstellen wir eine neue Zeichenkette, die *Hello World* enthält und rufen (auch bekannt als call oder execute) diese Funktion auf, wobei die Zeichenkette das erste und einzige Argument ist.

An diesem Punkt haben wir bereits viele neue Terminologien kennengelernt und du bist vielleicht etwas überfordert. Manchmal ist es hilfreich, dein Programm bewusst laut vorzulesen. Eine Lesung des Programms, das wir gerade geschrieben haben, könnte so ablaufen:

Es wird ein neues ausführbares Programm erstellt, das die fmt Bibliothek referenziert und eine Funktion namens main enthält. Diese Funktion nimmt keine Argumente an, gibt nichts zurück und führt folgende Schritte: Rufe die Println-Funktion aus dem fmt-Paket auf und führe sie mit einem Argument aus - der Zeichenkette Hello World.

Die Println-Funktion übernimmt die eigentliche Arbeit in diesem Programm. Du kannst mehr darüber herausfinden,indem du das Folgende in dein Terminal eingibst:

```plain
$ godoc fmt Println
```

Unter anderem solltest du das hier sehen:

```txt
    Println formats using the default formats for its operands and writes to
    standard output. Spaces are always added between operands and a newline
    is appended. It returns the number of bytes written and any write error
    encountered.
```

Go ist eine sehr gut dokumentierte Programmiersprache, aber diese Dokumentation kann schwierig zu interpretieren sein, es sei denn, du bist bereits einigermaßen mit Programmiersprachen und dem dazugehörigen Jargon vertraut. (Englischkenntnisse sind natürlich ebenfalls von Vorteil) Dennoch ist der godoc-Befehl äußerst nützlich und ein guter Ausgangspunkt, wenn du eine Frage hast.

Zurück zur vorliegenden Funktion! Diese Dokumentation sagt aus, dass die Println-Funktion alle Parameter, die du ihr gibst, an die Standardausgabe sendet - ein Namen für die Ausgabe des Terminals, in dem du arbeitest. Diese Funktion bewirkt, dass Hello World in deinem Terminal angezeigt wird.

Im nächsten Abschnitt werden wir untersuchen, wie Go Dinge wie Hello World speichert und darstellt, indem wir mehr über Typen lernen.

## Überprüf' Dein Wissen

- Was ist "Whitespace"?
- Was ist ein Kommentar? Welche Arten Kommentaren zu schreiben gibt es?
- Unser Programm begann mit `package main`. Womit würden die Dateien im fmt-Paket beginnen?
- Wir haben die im `fmt`-Paket definierte `Println`-Funktion verwendet. Wenn wir die `Exit`-Funktion aus dem `os`-Paket verwenden wollten, was müssten wir tun?
- Ändere das von uns geschriebene Programm so, dass es nicht `Hello World` ausgibt, sondern `Hello, my name is`, von deinem Namen gefolgt.