# 1. Typen

<!-- TOC -->

- [1. Typen](#1-typen)
  - [1.1. Nummern](#11-nummern)
    - [1.1.1. Ganzzahlen](#111-ganzzahlen)
  - [1.2. Eine Veranschaulichung der Zählsysteme Base 10 und Base 2](#12-eine-veranschaulichung-der-z%C3%A4hlsysteme-base-10-und-base-2)
    - [1.2.1. Base 10 - Dezimal](#121-base-10---dezimal)
    - [1.2.2. Base 2 - Binary](#122-base-2---binary)
    - [1.2.3. Gleitkommazahlen](#123-gleitkommazahlen)
  - [1.3. Beispiel](#13-beispiel)
  - [1.4. Strings](#14-strings)
  - [1.5. Booleans](#15-booleans)
  - [1.6. Überprüf' Dein Wissen](#16-%C3%BCberpr%C3%BCf-dein-wissen)

Im letzten Abschnitt haben wir den Datentyp `string` verwendet, um `Hello World` zu speichern.<br/>
Datentypen kategorisieren einen Satz verwandter Werte, beschreiben die Operationen, die mit ihnen durchgeführt werden können, und definieren, wie sie gespeichert werden. Da Typen ein schwer zu begreifendes Konzept sein können, werden wir sie aus verschiedenen Perspektiven betrachten, bevor wir sehen, wie sie in Go umgesetzt werden.

Gelegentlich unterscheiden philosophisch veranlagte Denker zwischen Typen und Token. Nehmen wir zum Beispiel an, du hast einen Hund namens Kastania. Kastania ist der Token (eine bestimmte Instanz oder ein bestimmtes Mitglied) und Hund ist der Typ (das allgemeine Konzept). "Hund" oder " Hundlichkeit " beschreibt eine Reihe von Eigenschaften, die alle Hunde gemeinsam haben. Obwohl übertrieben einfach, könnten wir so argumentieren: Alle Hunde haben 4 Beine, Kastania ist ein Hund, also hat Kastania 4 Beine. Typen in Programmiersprachen funktionieren ähnlich: Alle Zeichenketten haben eine Länge, `x` ist eine Zeichenkette, daher hat `x` eine Länge.

In der Mathematik sprechen wir oft über Mengen. Zum Beispiel: ℝ (die Menge aller reellen Zahlen) oder ℕ (die Menge aller natürlichen Zahlen). Jedes Element dieser Mengen teilt sich die Eigenschaften mit allen anderen Elementen der Menge. Zum Beispiel sind alle natürlichen Zahlen assoziativ: "für alle natürlichen Zahlen **a, b, und c, a + (b + c) = (a + b) + c und a × (b × c) = (a × b) × c.**". Auf diese Weise sind Mengen den Typen in Programmiersprachen ähnlich, da alle Werte eines bestimmten Typs bestimmte Eigenschaften teilen.

Go ist eine statisch typisierte Programmiersprache. Das bedeutet, dass Variablen immer einen bestimmten Typ haben und sich dieser Typ nicht ändern kann. Statische Typisierung kann zunächst umständlich erscheinen. Du wirst einen großen Teil deiner Zeit damit verbringen, dein Programm so zu korrigieren, dass es endlich kompiliert wird. Aber Typen helfen uns, darüber nachzudenken, was unser Programm macht, und helfen uns, eine Vielzahl von häufigen Fehlern zu erkennen.

Go verfügt über mehrere eingebaute Datentypen, die wir nun näher betrachten werden.

## 1.1. Nummern

Go hat mehrere verschiedene Typen, um Zahlen darzustellen. Im Allgemeinen teilen wir Zahlen in zwei verschiedene Arten auf: Ganzzahlen und Gleitkommazahlen.

### 1.1.1. Ganzzahlen

Ganzzahlen sind - wie ihr mathematisches Gegenstück - Zahlen ohne Dezimalkomponente. (...., -3, -2, -1, 0, 1, ....)

Im Gegensatz zum Dezimalsystem der Basis 10, mit dem wir Zahlen darstellen, verwenden Computer ein Binärsystem der Basis 2.

Unser System besteht aus 10 verschiedenen Ziffern. Sobald wir unsere verfügbaren Ziffern ausgeschöpft haben, stellen wir größere Zahlen dar, indem wir 2 (dann 3, 4, 5,...) Ziffern nebeneinander stellen. Zum Beispiel ist die Zahl nach 9 10, die Zahl nach 99 100 und so weiter. Computer tun das Gleiche, aber sie haben nur 2 statt 10 Ziffern. Das Zählen sieht also so aus: 0, 1, 10, 11, 100, 101, 110, 111 und so weiter. Der andere Unterschied zwischen dem Zahlensystem, das wir verwenden, und dem, das ein Computer verwendet, besteht darin, dass alle Ganzzahlentypen eine bestimmte Größe haben. Sie haben nur Platz für eine bestimmte Anzahl von Ziffern. Eine 4-Bit-Ganzzahl könnte also so aussehen: 0000, 0001, 0010, 0011, 0100. Irgendwann geht uns der Platz aus und die meisten Computer fangen einfach wieder am Anfang an. (Was zu sehr seltsamem Verhalten führen kann)

Die Ganzzahlentypen in Go sind: *uint8*, *uint16*, *uint32*, *uint64*, *int8*, *int16*, *int32* und *int64*. 8, 16, 32 und 64 sagen uns, wie viele *Bits* jeder der Typen verwendet. uint bedeutet "*unsigned integer*", während int "*signed integer*" bedeutet. Ganzzahlen ohne Vorzeichen enthalten nur positive Zahlen (oder Null). Zusätzlich gibt es zwei Alias-Typen: *Byte* -das gleich ist wie uint8- und *Rune* -das gleich ist wie int32. *Bytes* sind eine sehr gebräuchliche Maßeinheit, die auf Computern verwendet wird (**1 Byte = 8 Bit**, 1024 Byte = 1 Kilobyte, 1024 Kilobyte = 1 Megabyte,....) und deshalb wird Go's Byte-Datentyp oft bei der Definition anderer Typen verwendet. Es gibt auch 3 Rechner-abhängige Ganzzahlentypen: uint, int und uintptr. Sie sind vom Computer abhängig auf dem sie verwendet werden, da ihre Größe von der Art der verwendeten Architektur abhängt.

Generell solltest du einfach den `int` Typ verwenden, wenn du mit ganzen Zahlen arbeitest.

## 1.2. Eine Veranschaulichung der Zählsysteme Base 10 und Base 2

### 1.2.1. Base 10 - Dezimal

Base 10 ist das Zählsystem, was wir aus unserem Alltag kennen.

Dezimale Zahlen verwenden Ziffern von 0..9.

Ziffer ist größer als 9 -> Neue Ziffer vor der vorhergegangenen.

9 + 1 -> 10,

99 + 1 -> 100,

9969 + 1 -> 9970,

9999 + 1 -> 10000,

etc...

| Tabelle base 10 | 1.  | 2.  | 3.  | 4.   | 5.    | 6.     | 7.      | 8.       |
|-----------------|-----|-----|-----|------|-------|--------|---------|----------|
| Potenz          | 10⁰ | 10¹ | 10² | 10³  | 10⁴   | 10⁵    | 10⁶     | 10⁷      |
| Wertigkeit      | 1   | 10  | 100 | 1000 | 10000 | 100000 | 1000000 | 10000000 |

Wir können die Zahl 255 im Base 10 System z.B. wie folgt repräsentieren:

| 255 In Base 10 | 1.  | 2.  | 3.  | 4.   | 5.    | 6.     | 7.      | 8.       |
|----------------|-----|-----|-----|------|-------|--------|---------|----------|
| Potenz         | 10⁰ | 10¹ | 10² | 10³  | 10⁴   | 10⁵    | 10⁶     | 10⁷      |
| Wertigkeit     | 1   | 10  | 100 | 1000 | 10000 | 100000 | 1000000 | 10000000 |
| Anzahl         | 5   | 5   | 2   | 0    | 0     | 0      | 0       | 0        |

**255 = (2*10^2)+(5*10^1)+(5*10^0) = (2*100)+(5*10)+(5*1)**

### 1.2.2. Base 2 - Binary

Base 2 wird vorrangig in der Digitaltechnik verwendet. Der Grund dafür liegt im Aufbau eines Prozessors, der Transistoren hat die entweder ein- oder ausgeschaltet sein können, und somit ein binäres Wertepaar repräsentieren.

Binärzahlen verwenden nur die Ziffern 0 und 1.

Ziffer ist größer als 1 -> Neue Ziffer hinter der vorhergegangenen.

Bei 4 Bits:

0000 -> 0001 = Dezimal 1,

0001 -> 0010 = Dezimal 2,

0010 -> 0011 = Dezimal 3,

0011 -> 0100 = Dezimal 4,

0100 -> 0101 = Dezimal 5,

etc.

| Tabelle base 2, 8 Bit (1 Byte) | 1. | 2. | 3. | 4. | 5. | 6. | 7. | 8.  |
|--------------------------------|----|----|----|----|----|----|----|-----|
| Potenz                         | 2⁰ | 2¹ | 2² | 2³ | 2⁴ | 2⁵ | 2⁶ | 20⁷ |
| Wertigkeit                     | 1  | 2  | 4  | 8  | 16 | 32 | 64 | 128 |

Wir können die Zahl 255 im Base 2 System mit 8 Bits in nur einer Form repräsentieren:

| Tabelle base 2, 8 Bit (1 Byte) | 1. | 2. | 3. | 4. | 5. | 6. | 7. | 8.  |
|--------------------------------|----|----|----|----|----|----|----|-----|
| Potenz                         | 2⁰ | 2¹ | 2² | 2³ | 2⁴ | 2⁵ | 2⁶ | 20⁷ |
| Wertigkeit                     | 1  | 2  | 4  | 8  | 16 | 32 | 64 | 128 |
| On oder Off                    | On | On | On | On | On | On | On | On  |

**255 = 2^0+2^1+2^2+2^3+2^4+2^5+2^6+2^7 = 1+2+4+8+16+32+64+128**

### 1.2.3. Gleitkommazahlen

*Gleitkommazahlen* sind Zahlen, die eine *Dezimalkomponente* enthalten (*reelle Zahlen*). (1.234, 123.4, 0.00001234, 12340000) Ihre tatsächliche Darstellung auf einem Computer ist ziemlich kompliziert und ein Verständnis davon nicht wirklich notwendig, um zu wissen, wie man sie benutzt. Deshalb brauchen wir im Moment nur das Folgende zu beachten:

- Gleitkommazahlen sind ungenau. Gelegentlich ist es nicht möglich, eine Zahl darzustellen. Zum Beispiel ergibt die Berechnung 1,01 - 0,99 den Wert 0,020000000000000000000000000018 - Eine Zahl, die dem, was wir erwarten würden sehr nahe kommt, aber nicht genau dem Erwartungswert entspricht.

- Wie ganze Zahlen haben Gleitkommazahlen eine bestimmte Größe (32 Bit oder 64 Bit). Die Verwendung einer größeren Gleitkommazahl erhöht die Genauigkeit. (wie viele Ziffern es darstellen kann)

- Neben den Zahlen gibt es noch weitere Werte, die dargestellt werden können: "Not a Number" (NaN, für Dinge wie 0/0) und positive und negative Unendlichkeit. (+∞ und -∞)

Go hat zwei Gleitkomma-Typen: `float32` und `float64` (auch oft als **Single Precision** bzw. **Double Precision** bezeichnet) sowie zwei weitere Typen zur Darstellung komplexer Zahlen (Zahlen mit Imaginärteilen): `complex64` und `complex128`. Generell sollten wir bei der Arbeit mit Gleitkommazahlen bei `float64` bleiben.

## 1.3. Beispiel

Laß uns ein Beispielprogramm mit Zahlen schreiben.</br>
Erstelle zuerst einen Ordner namens b_typen in dem Verzeichnis ``~/go/src/ag/konzepte``, falls er noch nicht existiert, und erstelle eine `main.go`-Datei:

```plain
$ mkdir ~/go/src/ag/konzepte
$ cd ~/go/src/ag/konzepte
$ touch main.go
$ code .
```

Und fülle die Datei main.go mit folgendem Inhalt:

```go
package main

import "fmt"

func main() {
  fmt.Println("1 + 1 =", 1 + 1)
}
```

Wenn du das Programm ausführst solltest du folgendes sehen:

```sh
$ go run main.go
1 + 1 = 2
```

Man beachte, dass dieses Programm dem Programm, das wir in Abschnitt **a** geschrieben haben, sehr ähnlich ist. Es enthält die gleiche `package`-Zeile, die gleiche `import`-Zeile, die gleiche Funktionsdeklaration `func main` und verwendet die gleiche `Println`-Funktion. Diesmal geben wir statt der Zeichenkette `Hello World` die Zeichenkette `1 + 1 =` gefolgt vom Ergebnis des Ausdrucks `1 + 1` ans Terminal aus. Dieser Ausdruck besteht aus drei Teilen: dem numerischen Literal `1` (das vom Typ int ist), dem Operator `+` (der Addition bedingt) und einem weiteren numerischen Literal `1`.<br/>
Laß uns das Gleiche mit Gleitkommazahlen versuchen:

```go
fmt.Println("1 + 1 =", 1.0 + 1.0)
```

Es ist zu beachten, dass wir die *.0* verwenden, um Go mitzuteilen, dass es sich um eine Gleitkommazahl anstelle einer ganzen Zahl handelt. Wenn du dieses Programm ausführst, bekommst du das gleiche Ergebnis wie vorher.

Zusätzlich zum Additiv hat Go noch einige weitere arithmetische Operatoren:

| Zeichen | Name           |
|---------|----------------|
| +       | Addition       |
| -       | Subtraktion    |
| *       | Multiplikation |
| /       | Division       |
| %       | Rest           |

## 1.4. Strings

Wie wir in Abschnitt **a** gesehen haben, ist eine String eine Folge von Zeichen mit einer bestimmten Länge, die zur Darstellung von Text verwendet wird. Go-Strings bestehen aus einzelnen Bytes, in der Regel eines für jedes Zeichen. (Zeichen aus anderen Sprachen wie Chinesisch werden durch mehr als ein Byte dargestellt.)

String-Literale können mit doppelten Anführungszeichen `"Hello World"` oder mit Back-Ticks \`Hello World\` erstellt werden. Der Unterschied besteht darin, dass doppelte Anführungszeichenketten keine Leerzeilen enthalten dürfen und dass sie spezielle Escape-Sequenzen erlauben. Zum Beispiel wird ``\n`` durch eine neue Zeile und ``\t`` durch ein Tabulatorzeichen ersetzt.

Zu den häufigsten Operationen mit Zeichenketten gehören das Finden der Länge einer Zeichenkette: `len("Hello World")`, der Zugriff auf ein einzelnes Zeichen in der Zeichenkette: ``"Hello World"[1]``, und zwei Zeichenketten zusammenfügen (Verkettung): `"Hello " + "Wordl"`. Ändern wir das Programm, das wir zuvor erstellt haben, um dies auszuprobieren:

```go
package main

import "fmt"

func main() {
  fmt.Println(len("Hello World"))
  fmt.Println("Hello World"[1])
  fmt.Println("Hello " + "World")
}
```

Ein paar Dinge, die du beachten solltest:

- Ein Leerzeichen wird auch als Zeichen betrachtet, so dass die Länge der Zeichenkette 11 statt 10 beträgt und die dritte Zeile ``"Hello "`` statt ``"Hello"`` aufweist.
- Zeichenketten werden "indiziert", beginnend bei **0** und nicht bei 1. ``[1]`` gibt einem das 2. Element, nicht das 1. Element, wie man vielleicht annehmen würde. Außerdem ist zu beachten, dass du **101** statt **e** siehst, wenn du dieses Programm ausführst. Dies liegt daran, dass das Zeichen durch ein Byte dargestellt wird (denk' daran, dass ein **Byte** eine ganze Zahl ist - nämlich ein **int8**).
- Eine Möglichkeit, über die Indexierung nachzudenken, wäre so : ``"Hello World "1``. Das würdest du als **"Der zweite Buchstabe der Zeichenkette Hello World"** lesen.

Die *Verkettung* verwendet das gleiche Symbol wie die Addition. Der Go-Compiler findet  basierend auf den Typen der Argumente heraus, was zu tun ist. Da beide Seiten des ``+`` Zeichenketten sind, geht der Compiler davon aus, dass du eine Verkettung und keine Addition meinst. (Addition ist für Zeichenketten bedeutungslos)

## 1.5. Booleans

Ein *boolescher* Wert (benannt nach George Boole) ist ein spezieller 1-Bit-Integer-Typ, der verwendet wird, um wahr und falsch (oder ein- und ausgeschaltet) darzustellen. Es werden drei logische Operatoren mit booleschen Werten verwendet:

| Zeichen | Name  |
|---------|-------|
| &&      | und   |
| \|\|    | oder  |
| !       | nicht |

Hier ist ein Beispielprogramm, das zeigt, wie sie verwendet werden können:

```go
func main() {
  fmt.Println(true && true)
  fmt.Println(true && false)
  fmt.Println(true || true)
  fmt.Println(true || false)
  fmt.Println(!true)
}
```

Die Ausführung dieses Programms sollte dir folgendes anzeigen:

```plain
$ go run main.go
true
false
true
true
false
```

In der Regel werden *Wahrheitstabellen* verwendet, um zu definieren, wie diese Operatoren funktionieren:

| Ausdruck       | Wahrheitswert |
|----------------|---------------|
| true && true   | true          |
| true && false  | false         |
| false && true  | false         |
| false && false | false         |

| Ausdruck         | Wahrheitswert |
|------------------|---------------|
| true \|\| true   | true          |
| true \|\| false  | true          |
| false \|\| true  | true          |
| false \|\| false | false         |

| Ausdruck | Wahrheitswert |
|----------|---------------|
| !true    | false         |
| !false   | true          |

Dies sind die einfachsten Typen, die in Go enthalten sind. Sie bilden das Fundament, auf dem alle nachfolgenden Typen aufgebaut sind.

## 1.6. Überprüf' Dein Wissen

- Wie werden Ganzzahlen auf einem Computer gespeichert?
- Wir wissen, dass (in Base 10) die größte 1-stellige Zahl 9 und die größte 2-stellige Zahl 99 ist. Da in Binärform die größte 2-stellige Zahl 11 (3) ist, ist die größte 3-stellige Zahl 111 (7) und die größte 4-stellige Zahl 1111 (15) was ist die größte 8-stellige Zahl? (Hinweis: 10¹-1 = 9 und 10²-1 = 99)
- Obgleich mit der Aufgabe unterforder (in dieser simplen Form, jedenfalls), kannst du Go als Taschenrechner verwenden. Schreib ein Programm, das 10001 × 424242 berechnet und an das Terminal ausgibt. (Verwende den *-Operator für die Multiplikation)
- Was ist eine Zeichenkette? Wie ermittelt man ihre Länge?
- Was ist der Wahrheitswert dieses Ausdrucks?</br> **(true && false) || (false && true) || !(false && false)**
