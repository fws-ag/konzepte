# Pointer

Wenn wir eine Funktion aufrufen, die ein Argument entgegennimmt, wird dieses Argument in die Funktion kopiert:

```go
func zero(x int) {
	x = 0 // setze den Wert des Arguments auf 0
}

func main() {
  x := 5
  zero(x)
  fmt.Println(x) // x ist immernoch 5
}
```

In diesem Fall ändert die Nullfunktion die ursprüngliche x-Variable in der Hauptfunktion nicht.
Aber was, wenn wir es wollten?
Eine Möglichkeit, dies zu tun, ist die Verwendung eines speziellen Datentyps,
der als Pointer bekannt ist:

```go
func zeroPtr(xPtr *int) {
	*xPtr = 0
}

func main() {
  x := 5
  zeroPtr(&x)
  fmt.Println(x) // x ist 0
}
```

Pointer verweisen auf eine Stelle im Speicher,
an der ein Wert gespeichert ist, statt auf den Wert selbst. (Sie zeigen auf etwas)
Durch die Verwendung eines Pointers (*int) ist die zero Funktion in der Lage,
die ursprüngliche Variable zu ändern.

## Die Operatoren **\*** und **&**

In Go wird ein Pointer durch das Zeichen * (Sternchen) gefolgt vom Typ des gespeicherten Wertes dargestellt.
In der zeroPtr Funktion ist `xPtr` ein Zeiger auf einen int.

`*` wird auch zur "Dereferenzierung" von Pointer-Variablen verwendet. Die Dereferenzierung eines Pointers gibt uns Zugriff auf den Wert, auf den der Pointer zeigt. Wenn wir *`xPtr` = 0 schreiben, sagen wir: "Speichern Sie die int 0 an der Speicherposition, auf die sich `xPtr` bezieht". Wenn wir `xPtr` = 0 versuchen, erhalten wir stattdessen einen Compilerfehler, da ``xPtr`` kein `int` ist, sondern ein `*int`, dem nur ein weiterer `*int` Wert zugewiesen werden kann.

Den `&`-Operator verwenden wir, um die Adresse einer Variablen zu finden.
`&x` gibt ein `*int` (Pointer auf ein int) zurück, da `x` ein int ist. Dies ist es, was uns erlaubt,
die ursprüngliche Variable zu ändern. `&x`in `main` und `xPtr` in `zeroPtr` beziehen sich auf den gleichen Speicherort.

## Die **new** Funktion

Eine weitere Möglichkeit, um einen Pointer zu erhalten ist die eingebaute `new`-Funktion:

```go
func eins(xPtr *int) {
	*xPtr = 1
}

func main() {
	xPtr := new(int)
	eins(xPtr)
	fmt.Println(*xPtr) // xPtr ist 1
}
```

`new` nimmt einen Typ als Funktionsparameter, weist genügend Speicher zu, um einen Wert dieses Typs aufzunehmen (bei einem int8 z.B. 8 bit...), und gibt einen Pointer darauf zurück.

In einigen Programmiersprachen gibt es einen signifikanten Unterschiedzwischen der Verwendung von new und &, wobei große Sorgfalt erforderlich ist, um letztlich alle mit new erstellten Daten zu löschen. In Go ist es anders, die Programmiersprache verfügt über einen **garbage-collector**, was bedeutet, dass der Speicher automatisch bereinigt wird, wenn sich nichts mehr darauf bezieht.

Pointer werden bei Go's eingebauten (primitiven  - im Sinne von grundlegenden) Typen selten verwendet,
aber wie du im nächsten Abschnitt, **structures**, sehen wirst, sind sie äußerst nützlich,
wenn sie mit **Strukturen** gepaart werden.

## Teste dein Wissen

- Wie erhältst du die Speicheradresse einer Variablen?
- Wie weist du einem Pointer einen Wert zu?
- Wie erstellst du einen neuen Pointer?
- Was ist der Wert von `x` nachdem folgendes Programm ausgeführt wurde:

    ```go
    func square(x *float64) {
    *x = *x * *x
    }
    func main() {
    x := 1.5
      square(&x)
    }
    ```

- Schreib' ein Programm, das zwei ganze Zahlen miteinander vertauschen kann. `x := 5; y := 10; swap(&x, &y)` sollte `x = 10` und `y = 5` zur Folge haben...