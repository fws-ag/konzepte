# STRUKTUREN UND SCHNITTSTELLEN

Obwohl es uns möglich wäre, Programme nur mit den eingebauten Datentypen von Go zu schreiben, würde es irgendwann ziemlich mühsam werden.
Stell' Dir ein Programm vor, das mit Formen interagiert:

```go
func distance(x1, y1, x2, y2 float64) float64 {
	a := x2 - x1
	b := y2 - y1
	return math.Sqrt(a*a + b*b)
}

func rectangleArea(x1, y1, x2, y2 float64) float64 {
	l := distance(x1, y1, x1, y2)
	w := distance(x1, y1, x2, y1)
	return l * w
}

func circleArea(x, y, r float64) float64 {
	return math.Pi * r * r
}

func main() {
	var rx1, ry1 float64 = 0, 0
	var rx2, ry2 float64 = 10, 10
	var cx, cy, cr float64 = 0, 0, 5

	fmt.Println(rectangleArea(rx1, ry1, rx2, ry2))
	fmt.Println(circleArea(cx, cy, cr))
}
```

Die Übersicht über sämtliche Koordinaten zu behalten macht es schwierig zu erkennen,
was das Programm tut und wird mit hoher Wahrscheinlichkeit zu Fehlern führen.

## Strukturen

Eine einfache Möglichkeit, dieses Programm besser zu machen, ist die Verwendung einer *Struktur*.
Eine Struktur ist ein Typ, der benannte Felder enthält.
Zum Beispiel könnten wir einen Kreis wie folgt darstellen:

```go
type Kreis struct {
  x float64
  y float64
  r float64
}
```

Das Schlüsselwort *type* leitet einen neuen Typ ein. Es folgt der Name des Typs (Kreis), das Schlüsselwort struct, um anzuzeigen, dass wir einen Strukturtyp und eine Liste von Feldern innerhalb von geschweiften Klammern definieren. Jedes Feld hat einen Namen und einen Typ. Wie bei Funktionensargumenten können wir Felder, die den gleichen Typ haben, zusammenfassen:

```go
type Kreis struct {
  x, y, r float64
}
```

### Initialisierung

Wir können eine *Instanz* unseres neuen Kreis-Typs auf verschiedene Weise erstellen, z.B.:

```go
var c Kreis
```

Wie bei anderen Datentypen wird dadurch eine lokale Kreis-Variable erstellt, die standardmäßig auf Null gesetzt ist. Für eine Struktur bedeutet Null, dass jedes der Felder auf den entsprechenden Nullwert gesetzt wird (0 für Ints, 0.0 für Floats, "" für Zeichenketten, nil für Pointer,....).
Wir können auch die new Funktion verwenden:

```go
k := new(Kreis)
```

Dadurch wird Speicher für alle Felder reserviert, jedes dieser Felder auf seinen Nullwert gesetzt und ein Pointer zurückgegeben. (*Kreis) Häufiger wollen wir jedem der Felder einen Wert geben. Wir können dies auf zwei Arten tun. Erstens so:

```go
c := Kreis{x: 0, y: 0, r: 9}
```

Zweitens könnten wir die Feldnamen auch weglassen, wenn wir die Reihenfolge kennen, in der sie definiert wurden:

```go
c := Kreis{0, 0, 9}
```

## Felder

Wir können mit dem Operator **.** auf Felder zugreifen:

```go
fmt.Println(c.x, c.y, c.r)
c.x = 10
c.y = 5
```

Ändern wir die ursprüngliche **circleArea**-Funktion so, dass sie die **Kreis**-Struktur verwendet:

```go
func circleArea(k Kreis) float64 {
  return math.Pi * k.r*k.r
}
```

In der main Funktion haben wir nun folgendes:

```go
func main() {
 // Weggeschnitten ...

 k := Kreis{0, 0, 5}
 fmt.Println(circleArea(c))

// Weggeschnitten ...
}
```

Eine Sache, die man beachten sollte, ist, dass Argumente in Go immer kopiert werden. Wenn wir versuchen würden, eines der Felder innerhalb der circleArea-Funktion zu ändern, würde dies die ursprüngliche Variable nicht verändern. (Siehe Skript zu Pointer) Aus diesem Grund würden wir die Funktion typischerweise so schreiben:

```go
func circleArea(k *Kreis) float64 { // <- Hier nun als Typ des Arguments Pointer zu Kreis
  return math.Pi * k.r*k.r
}
```

Und konsequent in main:

```go
func main() {
 // Weggeschnitten ...

 k := Kreis{0, 0, 5}
 fmt.Println(circleArea(&k)) // <- Hier nun als Argument ein Pointer zu c

// Weggeschnitten ...
}
```

## Methoden

Obwohl dies besser ist als die erste Version dieses Codes, können wir ihn durch die Verwendung einer speziellen Art von Funktion, die als *Methode* bezeichnet wird, deutlich verbessern:

```go
func (k *Kreis) area() float64 {
  return math.Pi * k.r*k.r
}
```

Zwischen dem Schlüsselwort func und dem Namen der Funktion haben wir einen "Empfänger" hinzugefügt. Der Empfänger ist wie ein Argument- er hat einen Namen und einen Typ - aber durch die Erstellung der Funktion auf diese Weise können wir sie mit dem Operator **.** aufrufen:

```go
fmt.Println(c.area())
```

Dies ist viel einfacher zu lesen, wir brauchen den &-Operator nicht mehr (Go weiß automatisch, dass für diese Methode ein Pointer auf den Kreis als Argument verwendet wird) und da diese Funktion nur bei Kreisen verwendet werden kann, können wir der Funktion problemlos einen generischen Namen wie **area** geben.

Tun wir das Gleiche für das Rechteck:

```go
type Rechteck struct {
  x1, y1, x2, y2 float64
}

// Methode area für Struktur Rechteck
func (r *Rechteck) area() float64 {
  l := distance(r.x1, r.y1, r.x1, r.y2)
  w := distance(r.x1, r.y1, r.x2, r.y1)
  return l * w
}
```

In main schreiben wir:

```go
r := Rectangle{0, 0, 10, 10}
fmt.Println(r.area())
```

## Eingebettete Typen

Die Felder einer Struktur stellen in der Regel eine Art *Hat-Etwas* Beziehung dar. Zum Beispiel hat ein Kreis einen Radius.
Angenommen, wir hätten eine Personenstruktur:

```go
type Person struct {
  Name string
}

func (p *Person) Talk() {
  fmt.Println("Hi, my name is", p.Name)
}
```

Wenn wir nun eine neue Androidstruktur erstellen möchten, könnten wir folgendes tun:

```go
type Android struct {
  Person Person
  Model string
}
```

Das würde funktionieren, aber wir würden eher sagen, dass ein Android eine Person *ist*, als dass ein Android eine Person *hat*. Go unterstützt solche Beziehungen durch die Verwendung eines eingebetteten Typs, auch bekannt als anonyme Felder. Eingebettete Typen sehen so aus:

```go
type Android struct {
  Person
  Model string
}
```

Wir verwenden den Typ (**Person**) und geben ihm keinen Namen. Bei dieser Art der Definition kann auf die Struktur **Person** über den Typnamen zugegriffen werden:

```go
a := new(Android)
a.Person.Talk()
```

Aber wir können auch jede beliebige **Person**-Methode direkt auf bei Variablen des Typs **Android** aufrufen:

```go
a := new(Android)
a.Talk()
```

Diese *Ist-Etwas* Beziehung funktioniert dabei intuitiv: Personen können sprechen, ein Android ist eine Person, also kann ein Android sprechen.

## Schnittstellen

Du hast vielleicht bemerkt, dass wir die **area** Methode des Rechtecks genauso nennen konnten wie die **area** Methode des Kreises. Das war kein Zufall. Sowohl im wirklichen Leben als auch in der Programmierung sind solche Beziehungen alltäglich. Go hat eine Möglichkeit, diese zufälligen Ähnlichkeiten durch einen Typ, der als *Interface*, zu Deutsch *Schnittstelle*, bekannt ist, explizit zu machen. Hier ist ein Beispiel für eine **Form**-Schnittstelle:

```go
type Form interface {
  area() float64
}
```

Wie eine Struktur wird ein Interface mit dem *type* Keyword erstellt, gefolgt von einem Namen und dem Keyword *interface*. Aber anstatt Felder zu definieren, definieren wir ein "method set". Ein Methodenset ist eine Liste von Methoden, die ein Typ haben muss, um das Interface zu "implementieren".

In unserem Fall haben sowohl **Rechteck** als auch **Kreis** **area** Methoden, die float64s zurückgeben, so dass beide Typen das Form-Interface implementieren. An sich wäre das nicht besonders nützlich, aber wir können Interface-Typen als Argumente für Funktionen verwenden:

```go
//             Argument formen vom typ Form
func totalArea(formen ...Form) float64 {
  var fläche float64
  for _, s := range formen {
    fläche += s.fläche()
  }
  return area
}
```

Wir würden diese Funktion so aufrufen:

```go
fmt.Println(totalArea(&c, &r))
```

Schnittstellen können auch als Felder verwendet werden:

```go
type Mehrfachform struct {
  formen []Form
}
```

Wir können sogar Mehrfachform selbst in eine Form verwandeln, indem wir ihr eine Flächenmethode geben:

```go
func (m *Mehrfachform) area() float64 {
  var fläche float64
  for _, s := range m.formen {
    fläche += s.area()
  }
  return fläche
}
```

Nun kann eine Mehrfachform Kreise, Rechtecke oder sogar andere Mehrfachform enthalten.

## Übungen

- Gib an, was der Unterschied zwischen einer Methode und einer Funktion ist!
- Warum würde man eingebettete Typen - sprich anonyme Felder - anstelle normaler, benannter Felder verwenden?
- Füge der **Form** Schnittstelle eine neue Methode namens *Umkreis* hinzu, die den Umfang einer Form berechnet. Implementiere die Methode für **Kreis** und **Rechteck**.