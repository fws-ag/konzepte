package structs

import (
	"fmt"
	"time"

	// Importiert die CalcVec2D-Funktion sowie den Punkt-Struct
	functions "gitlab.com/konzepte/f_funktionen/beispiele"
)

// Structs, auch Structures oder Strukturen, sind eine Sammlung von Feldern.
// Sie fassen mehrere Werte mit primitive oder komplexe Typen zu einer logischen
// Einheit zusammen.
//
// Solch eine Einheit Könnte z.B. ein Datum sein:

// Datum ist eine Struktur, die ein Kalenderdatum repräsentiert.
type Datum struct {
	// Feld tag vom Typ int
	// Feld monat vom Typ time.Month
	// Feld jahr vom Typ int
	Tag   int
	Monat time.Month
	Jahr  int
}

// Phone repräsentiert eine Telefonnummer mit ihren drei typischen Komponenten
type Phone struct {
	Ländervorwahl   string
	Nationalvorwahl string
	Suffix          string
}

// Wir können die Struktur Phone nun auch als Typ verwenden:

// Mensch ist eine Struktur, die einige essentielle
// Charakteristiken eines Menschen zusammenfasst.
type Mensch struct {
	// Felder Vor -und Nachname vom Typ String
	// Feld Geburtsdatum vom Typ Datum
	Vorname      string
	Nachname     string
	Geburtsdatum Datum
	Tel          Phone
}

func main() {
	// Um eine Variable vom Typ unserer Struktur zu deklarieren, tun wir folgendes:
	mensch1 := Mensch{
		Vorname:  "Linus",
		Nachname: "Keiser",
		Geburtsdatum: Datum{
			Tag:   04,
			Monat: time.April,
			Jahr:  2000,
		},
		Tel: Phone{
			Ländervorwahl:   "49",
			Nationalvorwahl: "01523",
			Suffix:          "3777431",
		},
	}

	// Alle Felder, die durch die Struktur zusammengefasst werden müssen nun mit
	// Werten vom entsprechenden Typ versehen werden.

	// Wir können den Wert aller Felder der Variablen printen:
	fmt.Println(mensch1)

	// Oder den Wert eines bestimmten Feldes:
	fmt.Println(mensch1.Geburtsdatum)

	// Außerdem können wir per for loop über alle Felder iterieren,
	// und so den Index und Wert wiedergeben:

	// Wir können auch "anonyme" Strukturen deklarieren:
	punkt := struct {
		X int
		Y int
	}{
		X: 0,
		Y: 10,
	}

	fmt.Printf("Die Koordinaten des Punktes sind X %v | Y %v\n", punkt.X, punkt.Y)

	a := functions.Punkt{
		X: 4,
		Y: -6,
	}

	b := functions.Punkt{
		X: 2,
		Y: -8,
	}
	ab := functions.CalcVec2D(a, b)

	fmt.Println(ab)
}
