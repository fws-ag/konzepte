package main

import (
	"fmt"
	"math"
)

//  ----------------------  STRUKTUREN UND SCHNITTSTELLEN  ----------------------
// Dies ist die finale Version des Programmes, das in dem Skript für dieses
// Thema beschrieben ist. Lies dir erst das Skript durch!

func distance(x1, y1, x2, y2 float64) float64 {
	a := x2 - x1
	b := y2 - y1
	return math.Sqrt(a*a + b*b)
}

type Kreis struct {
	x, y, r float64
}

func (k *Kreis) area() float64 {
	return math.Pi * k.r * k.r
}

type Rechteck struct {
	x1, y1, x2, y2 float64
}

func (r *Rechteck) area() float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	w := distance(r.x1, r.y1, r.x2, r.y1)
	return l * w
}

type Form interface {
	area() float64
}

func totalArea(formen ...Form) float64 {
	var fläche float64
	for _, s := range formen {
		fläche += s.area()
	}
	return fläche
}

func main() {
	k := Kreis{0, 0, 9}
	fmt.Println(k.area())

	r := Rechteck{0, 0, 20, 20}
	fmt.Println(r.area())

	fmt.Println(totalArea(&k, &r))
}
