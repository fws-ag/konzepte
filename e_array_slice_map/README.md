# 1. Arrays, Slices und Maps

<!-- TOC -->
- [1. Arrays, Slices und Maps](#1-arrays-slices-und-maps)
	- [1.1. Arrays](#11-arrays)
	- [1.2. Slices](#12-slices)
	- [1.3. Slice Funktionen](#13-slice-funktionen)
	- [1.4. Maps](#14-maps)
	- [1.5. Überprüf' Dein Wissen](#15-%C3%BCberpr%C3%BCf-dein-wissen)

In Abschnitt **b** haben wir die grundlegenden Typen in Go kennengelern. In diesem Abschntt sehen wir uns nun drei weitere in Go integrierte Typen an: **Array**, **Slice** und **Map**.

## 1.1. Arrays

Ein Array ist eine durchnummerierte (*indizierte*) Folge von Elementen eines einzelnen Typs mit einer festen Länge. In Go sehen sie so aus:

```go
var x [10]int
```

`x` ist ein Beispiel für ein Array welches aus 10 Elementen vom Typ `int` besteht.<br/>
Führe das folgende Programm aus:

```go
package main

import "fmt"

func main() {
	var x [10]int
	x[5] = 100
	fmt.Println(x)
}
```

Der Output sollte `[0 0 0 0 0 100 0 0 0 0]` sein.
`x[5] = 100` solltest du lesen wie: "weise dem **sechsten** Element des Arrays x den Wert 100 zu".<br/>
Es mag seltsam erscheinen, dass x[5] das 6. und nicht das 5. Element repräsentiert, aber wie bei Zeichenketten werden Arrays ab 0 indiziert. Auf Arrays wird ähnlich zugegriffen wie auf Zeichenketten: wir könnten `fmt.Println(x)` in `fmt.Println(x[5])` ändern und würden 100 bekommen.

```go
package main

import "fmt"

func main() {

	array := [10]float64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10.76}

	var sum float64
	for i := 0; i < 10; i++ {
		sum += array[i]
		fmt.Println(array[i])
	}
	fmt.Println(sum / 10)
}
```

Dieses Programm berechnet den Durchschnitt einer Reihe von Werten. Wenn du es ausführst, solltest du 5.576 sehen. Laß uns das Programm durchgehen:

> - Erst deklarieren wir ein Array mit dem Name `array` das 10 Werte vom Typ `float64` beinhaltet.
>   In der Gleichen Zeile geben wir auch alle Werte an, die das Array beinhaltetn soll: `{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}`. Dies ist eine gängige Kurzform, um ein Array mit Initialwerten zu füllen.
> - Als nächstes deklarieren wir eine Variable mit dem Namen `sum`, ohne ihr explizit einen Wert zuzuweisen. Somit nimmt sie den Nullwert ihres Typs, float64, an, also 0.0.
> - Danach richten wir eine for-Schleife ein, um die Gesamtsumme zu berechnen.
> - Zum Schluss teilen wir die Gesamtsumme durch die Anzahl der Elemente, um den Durchschnitt zu erhalten.

Dieses Programm funktioniert, aber Go bietet einige Funktionen, mit denen wir es verbessern können. Zuerst diese 2 Bereiche: ``i < 5`` und ``sum / 5``. Dies sollte bei uns im übertragenen Sinne einen Alarm auslösen! Angenommen, wir haben die Anzahl der Werte von 10 auf 15 verändert... In diesem Fall müssten auch diese beiden Bereiche angepasst werden, was man schnell vergisst, gerade bei größeren Programmen als diesem. Es wäre besser, sich stattdessen auf die Länge des Arrays zu beziehen:

```go
	var sum float64
	for i := 0; i < len(array); i++ {
		sum += array[i]
		fmt.Println(array[i])
	}
	fmt.Println(sum / len(array))
```

Nimm diese Änderungen vor und führe das Programm aus. Du solltest einen Fehler erhalten:

```plain
$ go run main.go
# command-line-arguments
.\main.go:14:18: invalid operation: sum / len(array) (mismatched types float64 and int)
```

Das Problem ist, dass `len(array)` und `sum` nicht vom gleichen Typ sind. `sum` ist vom Typ float64, während len(array) einen int zurückgibt, wir müssen also ``len(array)`` in einen float64 konvertieren!<br/>
In Go funktioniert das so: `fmt.Println(total / float64(len(x)))`

Dies ist ein Beispiel für eine Typenkonvertierung. Im Allgemeinen verwendest du zum Konvertieren zwischen den Typen den Typnamen wie eine Funktion, z.B. eben von int zu float64 `float64(WERT)`...

Eine weitere Änderung am Programm, die wir vornehmen können, ist die Verwendung einer speziellen Form der for-Schleife:

```go
var total float64 = 0
for i, value := range x {
  total += value
}
fmt.Println(total / float64(len(x)))
```

In dieser for-Schleife stellt ``i`` die aktuelle Position in dem Array dar und ``value`` ist dasselbe wie ``array[i]``. Wir verwenden das Schlüsselwort ``range`` gefolgt vom Namen der Variablen, über die wir eine Schleife erzeugen möchten.

> **Das Range Keyword**<br/>
> ``range`` ist ein Ausdruck und liefert bei Arrays und Slices sowohl den Index als auch den Wert für jeden Eintrag.<br/>
> Das ``range``-Schlüsselwort wird in for-Schleifen verwendet, um über Elemente eines Arrays, Slice oder einer Map zu iterieren. <br/>
> Bei Arrays und Slices wird der Index des Elements als Integer zurückgegeben.<br/>
> Bei Maps wird der Key des nächsten Key-Value-Paares zurückgegeben.<br/>
> Range gibt entweder einen oder zwei Werte zurück. Wenn links von einem Range-Ausdruck nur ein Name verwendet wird, so nimmt er den **ersten** Wert in der folgenden Tabelle an:

| range-Ausdruck                            | 1. Wert                   | 2. Wert     (Optional) |
|-------------------------------------------|---------------------------|------------------------|
| Array oder Slice `a` vom Typ `[n]E`       | **index** `i` vom Typ int | Element `a[i]E`        |
| String `s`                                | **index** `i` vom Typ int | **rune** vom Typ int   |
| Map `m` vom Typ `map[key-type]value-type` | **key** `k`               | **value** `m[k]v`      |

Die Ausführung dieses Programms führt zu einem weiteren Fehler:

```plain
$ go run main.go
# command-line-arguments
.\main.go:10:6: i declared and not used
```

Der Go-Compiler erlaubt es dir nicht, Variablen zu erstellen, die du nie benutzt. Da wir ``i`` nicht innerhalb unserer Schleife verwenden, müssen wir es entsprechend ändern:

```go
var total float64 = 0
for _, value := range x {
  total += value
}
fmt.Println(total / float64(len(x)))
```

Ein einzelner _ (Unterstrich) wird verwendet, um dem Compiler mitzuteilen, dass wir etwas nicht benötigen. (In diesem Fall benötigen wir die Iterator-Variable nicht)

Go bietet auch eine kürzere Syntax für die Erstellung von Arrays, die wir ja gerade gesehen haben:

```go
array := [5]float64{ 98, 93, 77, 82, 83 }
```

Wenn wir nun einen einzelnen Wert des Arrays `array` verändern wollen nutzen wir die folgende Syntax, wie oben schon beschrieben:

```go
array := [10]float64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10.76}
array[0] = 1000
```

Manchmal können derartige Arrays zu lang werden, um auf eine Zeile zu passen, also lässt Go zu, dass man sie auf diese Art und Weise formatiert:

```go
array := [10]float64{
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10.76,
    }
```

Achte auf das zusätzliche Komma nach dem letzten Wert: `10.76,`!<br/>
Dies wird von Go gefordert und ermöglicht es uns z.B., ein Element einfach aus dem Array zu entfernen, indem wir die Zeile auskommentieren:

```go
array := [10]float64{
	1,
	2,
	3,
	4,
	5,
	//6,
	7,
	8,
	9,
	//10.76,
    }
```

## 1.2. Slices

Ein Slice ist ein Segment eines Arrays. So wie Arrays sind auch Slices indizierbar und haben eine Länge. Im Gegensatz zu Arrays darf sich diese Länge ändern. Hier ist ein Beispiel für ein Slice:

```go
var slice []float64
```

Der einzige Unterschied zu einem Array besteht in der fehlenden Länge zwischen den Brackets. In diesem Fall wurde slice mit einer Länge von 0 angelegt.

Wenn du ein Slice erstellen möchtest, solltest du die eingebaute ``make``-Funktion verwenden:

```go
slice := make([]float64, 10)
```

Dadurch entsteht ein Slice, das einem zugrundeliegenden float64-Array der Länge 10 zugeordnet ist. Slices sind immer einem Array zugeordnet, und obwohl sie nie länger als das Array sein können, können sie kleiner sein. Die make-Funktion erlaubt auch einen dritten Parameter:

```go
slice := make([]float64, 10, 20)
```


20 stellt die Kapazität des zugrunde liegenden Arrays dar, auf das das Slice zeigt.

Eine weitere Möglichkeit, Slices zu erstellen, besteht darin, den ``[anfang : ende]`` Ausdruck zu verwenden:

```go
array := [10]float64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
slice := array[0:5]
```

``anfang`` ist der Index, wo das Slice beginnen soll und ``ende`` ist der Index, wo es enden soll (aber auschließlich des Indexes selbst). Während beispielsweise array[0:5][1,2,3,4,5] zurückgibt, gibt array[1:4][2,3,4] zurück.

Der Einfachheit halber dürfen wir auch ``anfang``, `ende` oder sogar beide weglassen:<br/>
``array[0:]`` ist gleich wie ``array[0:len(array)]``,<br/>
``array[:9]`` ist das gleiche wie ``array[0:9]`` und<br/>
``array[:]`` ist gleichbedeutend mit ``array[0:len(array)]``.

## 1.3. Slice Funktionen

Go enthält zwei integrierte Funktionen zur Unterstützung von Slices: ``append`` und `copy`. Hier ist ein Beispiel für ``append``:

```go
func main() {
	slice1 := []int{1, 2, 3}
	slice2 := append(slice1, 4, 5)
	fmt.Println(slice1, slice2)
}
```

Nach dem Ausführen dieses Programms hat ``slice1`` ``[1,2,3]`` und ``slice2`` hat ``[1,2,3,4,5]``.<br/>
``append`` erstellt ein neues Slice, indem es ein bestehendes Slice (das erste Argument) nimmt und alle folgenden Argumente an es anhängt.

Hier ein Beispiel für `copy`:

```go
func main() {
	slice1 := []int{1, 2, 3}
	slice2 := make([]int, 2)
	copy(slice2, slice1)
	fmt.Println(slice1, slice2)
}
```

Nach der Ausführung dieses Programms hat ``slice1`` `[1,2,3]` und ``slice2`` ``[1,2]``. Der Inhalt von ``slice1`` wird in ``slice2`` kopiert, aber da ``slice2`` nur Platz für zwei Elemente hat, werden nur die ersten beiden Elemente von ``slice1`` übertragen.

## 1.4. Maps

Eine **Map** ist eine unsortierte Sammlung von **Schlüssel-Wert-Paaren**. Auch bekannt als **Assoziatives Array**, **Hash-Table** oder **Dictionary**, dienen Maps dazu, einen Wert mit einem zugehörigen Schlüssel zu speichern und später anhand dessen aufzurufen. Hier ist ein Beispiel für ein assoziatives Array in Go:

```go
var m map[string]int
```

Der ``map``-Typ wird durch das Keyword ``map`` dargestellt, gefolgt vom **Schlüsseltyp** in Klammern und schließlich vom **Werttyp**. Wenn du das laut vorlesen würdest, würdest du sagen: "x ist eine Map von Zeichenketten zu Ints.". "Von zu", weil die Map Werte vom Typ `string` mit Werten vom Typ `int` assoziert.

Wie bei Arrays und Slices kann auch auf Maps mit der Klammernotation zugegriffen werden. Probiere das folgende Programm aus:

```go
var m map[string]int
m["schlüssel"] = 1
fmt.Println(m)
```

Du solltest einen ähnlichen Fehler wie diesen sehen:

```plain
$ go run main.go
panic: assignment to entry in nil map

goroutine 1 [running]:
main.main()
        C:/Users/linus/go/src/ag/konzepte/e_array_slice_map/main.go:7 +0x56
exit status 2
```

Bisher haben wir nur **Kompilierungsfehler** gesehen. Dies ist ein Beispiel für einen **Laufzeitfehler**. Wie der Name schon sagt, treten bei der Ausführung des Programms Laufzeitfehler auf, während bei der Kompilierung des Programms Kompilierungsfehler auftreten.

Das Problem mit unserem Programm ist, dass Maps initialisiert werden müssen, bevor sie verwendet werden können. Wir hätten das hier schreiben sollen:

```go
m := make(map[string]int)
m["schlüssel"] = 1
fmt.Println(m["schlüssel"])
```

Wenn du dieses Programm ausführst, solltest du 1 angezeigt bekommen. Die Anweisung ``m["schlüssel"] = 1`` ist ähnlich wie bei Arrays, aber der Schlüssel -anstatt einer Ganzzahl- ist eine Zeichenkette, weil der Schlüsseltyp der Map string ist. Wir können auch Maps mit einem Schlüsseltyp von int(oder jedem anderen Typ...) erstellen:

```go
m := make(map[int]int)
m[9] = 1
```

Dies sieht ganz ähnlich aus wie ein Array, aber es gibt ein paar Unterschiede. Zuerst kann sich die Länge einer Map (gefunden durch len(m)) ändern, wenn wir neue Elemente hinzufügen. Beim ersten Erstellen hat sie eine Länge von 0, nach m[9] = 1 hat es eine Länge von 1.
Zweitens sind Maps nicht sequentiell. Wir haben m[9], was bei einem Array bedeuten würde, dass es ein m[8] geben muss, aber Maps haben diese Anforderung nicht.

Mit der eingebauten ``delete``-Funktion können wir Elemente aus einer Karte löschen:

```go
delete(m, 9)
```

Der erste Parameter ist die Map, aus der wir etwas löschen möchten, und der zweite ist der Schlüssel, dessen Schlüssel-Wert-Paar gelöscht werden soll.

Laß uns ein Beispielprogramm betrachten:

```go
package main

import "fmt"

func main() {
	hauptstädte := make(map[string]string)

	hauptstädte["de"] = "Berlin"
	hauptstädte["fr"] = "Paris"
	hauptstädte["ru"] = "Moskau"
	hauptstädte["us"] = "Washington, D.C."

	fmt.Println(hauptstädte["de"])
}
```

`hauptstädte` ist ein assoziatives Array, welches die Namen von Hauptstädte mit ihren abgekürzten Ländernamen verknüpft.

Dies ist eine sehr gebräuchliche Art der Verwendung von Karten: als Referenztabelle oder als Datenverzeichnis. Angenommen, wir würden versuchen, einen Schlüssel zu referenzieren, der nicht existiert:

```go
fmt.Println(hauptstädte["nl"])
```

Wenn du dies ausführst, wirst du nichts zurückerhalten. Technisch liefert eine Map den Nullwert für den Werttyp (bei Zeichenketten ist dies die Leerzeichenkette). Obwohl wir den Nullwert mit einem Konditionsausdruck überprüfen könnten (``hauptstödte["nl"] == ""``), bietet Go einen besseren Weg:

```go
name, ok := hauptstädte["nl"]
fmt.Println(name, ok)
```

Der Zugriff auf ein Element einer Map kann zwei Werte statt nur einen liefern. Der erste Wert ist das Ergebnis des Suchvorgangs, der zweite sagt uns, ob der Suchvorgang erfolgreich war oder nicht. In Go sehen wir oft Code wie diesen:

```go
if name, ok := hauptstädte["nl"]; ok {
  fmt.Println(name, ok)
}
```

Zuerst versuchen wir, den Wert aus der Map zu erhalten, und wenn es erfolgreich ist, führen wir den Code innerhalb des Blocks aus.

Wie wir bei Arrays gesehen haben, gibt es auch einen kürzeren Weg, um Maps zu erzeugen:

```go
	hauptstädte := map[string]string{
		"de":  "Berlin",
		"fr":  "Paris",
		"usa": "Washington, D.C.",
		"ru":  "Moskau",
	}
```

Karten werden auch häufig zur Speicherung allgemeiner Informationen verwendet. Ändern wir unser Programm so, dass wir nicht nur den Namen der Stadt speichern, sondern auch ihre Einwohnerzahl:

```go
package main

import "fmt"

func main() {
	hauptstädte := map[string]map[string]string{
		"de": map[string]string{
			"name":      "Berlin",
			"einwohner": "3575000",
		},
		"fr": map[string]string{
			"name":      "Paris",
			"einwohner": "2200000",
		},
		"usa": map[string]string{
			"name":      "Washington, D.C.",
			"einwohner": "693972",
		},
		"ru": map[string]string{
			"name":      "Moskau",
			"einwohner": "11920000",
		},
	}

	if el, ok := hauptstädte["de"]; ok {
		fmt.Println(el["name"], el["einwohner"])
	}
}
```

Es ist zu beachten, dass sich der Typ unserer Map von ``map[string]string`` zu ``map[string]map[string]string`` geändert hat. Wir haben jetzt eine *Zuordnung von Zeichenketten zu einer Zuordnung von Zeichenketten zu Zeichenketten* :) . Die äußere Map wird als Referenztabelle basierend auf dem Ländernamen der Stadt verwendet, während die inneren Maps verwendet werden, um allgemeine Informationen über die Städte zu speichern. Obwohl Maps häufig so verwendet werden, werden wir in Abschnitt **h** eine bessere Möglichkeit sehen, strukturierte Informationen zu speichern.

## 1.5. Überprüf' Dein Wissen

- Wie kommst du an das 5. Element eines Arrays oder Slice?
- Was ist die Länge des Slice `slice`? `slice := make([]int, 10, 20]`
- In Anbetracht des Arrays:

    ```go
    `arr := [4]string{"a", "b", "c", "d"}`
    ```

    Was würde `arr[1:4]` für einen Wert ergeben?
- Schreibe ein Programm, das die größte Zahl in dieser Liste findet:

    ```go
    list := []int{
        11, 67, 23, 54, 69,
        75, 4, 72, 92, 16,
        47, 97, 31, 26, 25,
        34, 45, 98, 15, 62,
    }
    ```

    Hinweis: denk' daran, dass du über alle Werte in einem Slice iterieren kannst, z.B. mit `range`...
