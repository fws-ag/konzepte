package main

import "fmt"

func main() {
	hauptstädte := map[string]map[string]string{
		"de": map[string]string{
			"name":      "Berlin",
			"einwohner": "3575000",
		},
		"fr": map[string]string{
			"name":      "Paris",
			"einwohner": "2200000",
		},
		"usa": map[string]string{
			"name":      "Washington, D.C.",
			"einwohner": "693972",
		},
		"ru": map[string]string{
			"name":      "Moskau",
			"einwohner": "11920000",
		},
	}

	if el, ok := hauptstädte["de"]; ok {
		fmt.Println(el["name"], el["einwohner"])
	}

	for i, r := range "Hello 🤗" {
		fmt.Println(i, r)
	}
}
