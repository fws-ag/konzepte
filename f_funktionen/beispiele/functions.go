package functions

// Eine Funktion besteht aus zwei Komponenten:
// 1. Signatur
// 2. Funktionskörper
//
// Die Signatur enthält Informationen über die Anzahl
// der Argumente und den Typ jedes Arguments, sowie die
// Anzahl der Ergebniswerte und ihren jeweiligen Typ.
// Sie stellt einen Vertrag dar, der nicht gebrochen werden
// kann.
//
// Der Funktionskörper enthält Code, der zur Verarbeitung
// der Argumente dient, so dass Ergebniss(e) produziert
// werden.

// Keyword Kennung(Argumentn TypArgumentn, ...) TypErgebniswert {
//	logik...
//}
func Add(a float64, b float64) float64 {
	ergebnis := a + b

	return ergebnis
}

// Eine Funktion, Variable oder Struktur ist öffentlich,
// wenn ihre Kennung mit einem Großbuchstaben beginnt
func IterateOverSlice(slice []int) map[int]int {
	pairs := map[int]int{}

	for i, v := range slice {
		pairs[i] = v
	}

	return pairs
}

// Punkt repräsentiert einen Punkt in einem 2-dimensionalen Koordinatensystem
type Punkt struct {
	X float64
	Y float64
}

// Vector2D repräsentiert einen 2-dimensionalen Vektor
type Vector2D struct {
	Spitze float64
	Fuss   float64
}

// CalcVec2D errechnet den Verbindungsvektor zwischen zwei Punkten a und b
// Der Ergebniswert ist der Vektor von A -> B.
func CalcVec2D(a Punkt, b Punkt) Vector2D {

	return Vector2D{
		Spitze: b.X - a.X,
		Fuss:   b.Y - a.Y,
	}
}
