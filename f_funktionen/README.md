# Funktionen

Eine Funktion ist ein eigenständiger Codeabschnitt, der null oder mehr Eingangsparameter auf null oder mehr Ausgangsparameter abbildet. Funktionen (auch Prozeduren oder Unterprogramme genannt) werden oft als Blackbox dargestellt: Werte kommen rein, Werte gehen raus, und zwischendrinnen passiert irgendetwas mit ihnen...

Bisher haben die Programme, die wir in Go geschrieben haben, nur eine Funktion verwendet:

`func main() {}`

Wir beginnen nun mit dem Schreiben von Programmen, die mehr als eine Funktion verwenden.

Erinnere dich kurz an dieses Programm aus Abschnitt **e**:

```go
package main

import "fmt"

func main() {

	array := [10]float64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10.76}

	var sum float64
	for i := 0; i < 10; i++ {
		sum += array[i]
		fmt.Println(array[i])
	}
	fmt.Println(sum / 10)
}
```

Dieses Programm berechnet den Durchschnitt einer Reihe von Zahlen. Das Finden des Durchschnitts ist ein sehr alltägliches Problem, so dass es ein idealer Kandidat für die Definition als Funktion ist.

Die `average`-Funktion muss eine Reihe von float64s aufnehmen und einen float64 zurückgeben. Dies ist vor der `main`-Funktion einzufügen:

```go
func average(values []float64) float64 {
  panic("Noch nicht implementiert")
}
```

Funktionen beginnen mit dem Schlüsselwort `func`, gefolgt vom Namen der Funktion. Die Parameter (Inputs) der Funktion werden wie folgt definiert: `(name typ, name typ, name typ, ...)`. Unsere Funktion hat einen Parameter (die Liste der Werte, deren Durschnitt wir errechnen möchten), den wir `values` genannt haben. Nach den Parametern setzen wir den Rückgabetyp. Zusammenfassend werden die Parameter und der Rückgabetyp als **Signatur** der Funktion bezeichnet.

Schließlich haben wir den Funktionskörper, der eine Reihe von Statements zwischen geschweiften Klammern behinhaltet. In diesem Block rufen wir eine eingebaute Funktion namens `panic` auf, die einen Laufzeitfehler verursacht. (Wir werden später in diesem Abschnitt mehr über `panic` erfahren) Das Schreiben von Funktionen kann schwierig sein, daher ist es eine gute Idee, den Prozess in überschaubare Teile zu unterteilen, anstatt zu versuchen, das Ganze in einem großen Schritt zu implementieren.

Implementieren wir die Funktionalität des Codes aus unserer `main`-Funktion in der `average`-Funktion:

```go
func average(values []float64) float64 {
  var total float64

  for _, v := range xs {
    total += v
  }
  return total / float64(len(xs))
}
```

Es ist zu beachten, dass wir `fmt.Println` in `return` umgeändert haben. Die `return`-Anweisung bewirkt, dass die Funktion sofort stoppt und den Wert, der danach steht, an die Funktion zurückgibt, die diese aufgerufen hat. Ändere `main` nun wie folgt:

```go
func average(values []float64) float64 {
  var total float64

  for _, v := range xs {
    total += v
  }
  return total / float64(len(xs))
}
```

```go
func main() {
  array := [10]float64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10.76}
  fmt.Println(average(array))
}
```

Die Ausführung dieses Programms sollte exakt das gleiche Ergebnis wie die ursprüngliche Version liefern. Ein paar Dinge, die du beachten solltest:

- Funktionen haben keinen Zugriff auf etwas in der aufrufenden Funktion. Das hier wird nicht funktionieren:

    ```go
    func f() {
      fmt.Println(x)
    }
    func main() {
      x := 5
      f()
    }
    ```

    wir müssten entweder dies:

    ```go
    func f(x int) {
      fmt.Println(x)
    }
    func main() {
      x := 5
      f(x)
    }
    ```

    Oder das tun:

    ```go
    var x int = 5
    func f() {
      fmt.Println(x)
    }
      func main() {
      f()
    }
    ```

- Wir können auch den Rückgabetyp benennen:

    ```go
    func f2() (r int) {
      r = 1
      return
    }
    ```

## Rückgabe mehrerer Werte

Go ist auch in der Lage, mehrere Werte aus einer Funktion zurückzugeben:

```go
func f() (int, int) {
  return 5, 6
}

func main() {
  x, y := f()
}
```

Drei Änderungen sind notwendig: Ändere den Rückgabetyp so, dass er mehrere Typen enthält, die durch `,` getrennt sind, ändere die Expression nach dem `return` so, dass sie mehrere Expressionen enthält, die durch `,` getrennt sind, und ändere schießlich noch die Deklarationanweisung so, dass mehrere Werte auf der linken Seite des `:=` oder `=` stehen.

Mehrere Rückgabewerte werden oft verwendet, um einen Fehlerwert zusammen mit dem Ergebnis zurückzugeben `(x, err:= f())`, oder ein Boolescher als Indikator für den Erfolg `(x, ok := f())`.

## Variadische Funktionen

Für den letzen Parameter einer Funktion steht in Go eine Sonderformulierung zur Verfügung:

```go
func add(args ...int) int {
  total := 0
  for _, v := range args {
    total += v
  }
  return total
}
func main() {
  fmt.Println(add(1,2,3))
}
```

Durch die Verwendung von `...` vor dem Typnamen des letzten Parameters kannst du angeben, dass es Null oder mehr dieser Parameter gibt. In diesem Fall gibt es Null oder mehr Ints. Wir rufen die Funktion wie jede andere Funktion auf, außer dass wir so viele Ints als Input geben können, wie wir wollen.

Genau so ist die Funktion fmt.Println implementiert:

`func Println(a ...interface{}) (n int, err error)`

Die Funktion `Println` nimmt beliebig viele Werte beliebigen Typs an. (Der spezielle `interface{}`-Typ) wird in Abschnitt **h** näher erläutert.

Wir können auch ein Slice von ints als Input verwenden, indem `...` dem Slice nachstellen:

```go
func main() {
  slice := []int{1,2,3}
  fmt.Println(add(slice...))
}
```

## Closures

Es ist möglich, Funktionen innerhalb von Funktionen zu erstellen:

```go
func main() {

  add := func(x, y int) int {
    return x + y
  }

  fmt.Println(add(1,1))
}
```

`add` ist eine lokale Variable vom Typ `func(int, int) int` (eine Funktion, die zwei Ints nimmt und ein Int zurückgibt). Wenn du eine solche **lokale Funktion** erstellst, hat sie auch Zugriff auf andere lokale Variablen (erinnere dich an den Geltungsbereich aus Abschnitt **c**):

```go
func main() {
  x := 0
  increment := func() int {
    x++
    return x
  }
  fmt.Println(increment())
  fmt.Println(increment())
}
```

`increment` fügt 1 zu der Variablen `x` hinzu, die im Scope der `main`-Funktion definiert ist. Auf diese `x`-Variable kann mit der `increment`-Closure zugegriffen werden. Deshalb sehen wir beim ersten Aufruf von `increment` 1, und beim zweiten Aufruf 2.


Eine solche Funktion zusammen mit den nicht-lokalen Variablen, auf die sie verweist, wird als **Closure** bezeichnet. In diesem Fall bilden `increment` und die Variable `x` die Closure.

Eine Möglichkeit zur Verwendung von Closures ist z.B. das Schreiben einer Funktion, die eine andere Funktion zurückgibt, die - wenn sie aufgerufen wird - eine Folge von Zahlen erzeugen kann. Zum Beispiel können wir wie folgt alle geraden Zahlen erzeugen:

```go
func generateEvenUints() func() uint {
  i := uint(0)
  return func() (ret uint) {
    ret = i
    i += 2
    return
  }
}

func main() {
  nextEven := generateEvenUints()
  fmt.Println(nextEven()) // 0
  fmt.Println(nextEven()) // 2
  fmt.Println(nextEven()) // 4
}
```

`generateEvenUints` gibt eine Funktion zurück, die gerade Zahlen erzeugt. Jedes Mal, wenn sie aufgerufen wird, fügt sie 2 zur lokalen `i`-Variablen hinzu, die - im Gegensatz zu normalen lokalen Variablen - zwischen den Aufrufen bestehen bleibt.

## Rekursion

Eine Funktion kann sich auch selbst aufrufen! Hier ist eine Möglichkeit, die Fakultät einer Zahl zu berechnen:

```go
func factorial(x uint) uint {
  if x == 0 {
    return 1
  }
  return x * factorial(x-1)
}
```

`factorial` ruft sich selbst auf, was diese Funktion rekursiv macht. Um besser zu verstehen, wie diese Funktion funktioniert, sollten wir einmal durchgehen was passiert, wenn wir `factorial` mit dem Parameter `2` aufrufen:

> - Ist `x == 0`? Nein! (x ist 2)
> - Ermittle die Fakultät von `x - 1`
>   - Ist `x == 0`? Ja!
>       - -> Gib' 1 wieder
>   - Gib' `1 * 1` wieder
> - Gib' `2 * 1` wieder

Closure und Rekursion sind mächtige Programmiertechniken, die die Grundlage für ein Paradigma bilden, das als funktionale Programmierung bekannt ist. Die meisten Menschen werden die funktionale Programmierung schwieriger zu verstehen finden als ein Ansatz, der auf for-Schleifen, if-Statements, Variablen und einfachen Funktionen basiert (Paradigma der prozeduralen Programmierung). Dennoch bietet das Paradigma der funktionalen Programmierung in vielen Situationen Vorteile, insbesondere was die Robustheit unserer Programme anbelangt. In späteren Abschnitten werden wir uns deshalb eingehender damit befassen.

## Defer, Panic & Recovery

Go hat eine spezielle Anweisung namens `defer`, die einen Funktionsaufruf plant, der nach Durchführung der Funktion ausgeführt wird. Schauen wir uns das folgende Beispiel an:

```go
package main

import "fmt"

func first() {
  fmt.Println("1.")
}
func second() {
  fmt.Println("2.")
}
func main() {
  defer second()
  first()
}
```

Dieses Programm gibt erst `1.` und dann `2.` ans Terminal aus. Im Grunde schiebt `defer` den Aufruf der Funktion `second` auf, bis das Ende der `main` Funktion erreicht ist.

Ohne `defer` müsste unsere `main` Funktion so aussehen, um das gleiche Ergebnis zu erzielen:

```go
func main() {
    first()
    second()
}
```

`defer` wird oft verwendet, wenn Ressourcen in irgendeiner Weise freigegeben werden müssen. Wenn wir zum Beispiel eine Datei öffnen, müssen wir sicherstellen, dass sie später geschlossen wird. Mit `defer` sähe das so aus:

```go
f, _ := os.Open(dateiname)
defer f.Close()
```

Dies bietet 3 Vorteile:

1. Es sorgt dafür, dass der Aufruf von `Close` nahe beim Aufruf von `Open` bleibt, was unser Programm übersichtlicher macht.
2. Wenn unsere Funktion mehrere `return`-Anweisungen hatte (vielleicht eine in einem `if` und eine in einem `else`) wird Close trotzdem garantiert vor Beiden aufgerufen.
3. Durch `defer` aufgeschobene Funktionen werden sogar bei einem Laufzeitfehler ausgeführt werden.

## Panic & Recover

Zuvor haben wir eine Funktion erstellt, die die `panic`-Funktion aufrief, um einen Laufzeitfehler zu verursachen. Wir können eine Laufzeitpanik mit der integrierten `recover`-Funktion handhaben. `recover` stoppt die Panik und gibt den Wert zurück, der an den Aufruf zu `panic` gegeben wurde. Wir könnten versucht sein, `recover` so zu benutzen:

```go
package main

import "fmt"

func main() {
  panic("PANIK")
  str := recover()
  fmt.Println(str)
}
```

Aber der Aufruf zu `recover` wird in diesem Fall nie erfolgen, da der Aufruf von `panic` die weitere Ausführung der Funktion sofort stoppt, wenn er passiert. Wir müssen es stattdessen mit `defer` koppeln:

```go
package main

import "fmt"

func main() {
  defer func() {
    str := recover()
    fmt.Println(str)
  }()
  panic("PANIK")
}
```

Eine Panik deutet im Allgemeinen auf einen Programmierfehler hin (z.B. der Versuch, auf einen Index eines Arrays zuzugreifen, der außerhalb der zulässigen Länge, das Versäumen, eine Map zu initialisieren, usw.) oder auf eine Ausnahmesituation, von der es keinen einfachen Weg zur "Rettung" gibt. (Daher der Name "Panik")

## Überprüf' Dein Wissen

- `sum` ist eine Funktion, die als Parameter ein Slice von Zahlen annimmt und den Inhalt addiert. Wie würde ihre Funktionssignatur aussehen?
- Schreibe eine Funktion, die eine **Ganzzahl** halbiert und `true` zurückgibt wenn die Ganzzahl gerade war, `false` wenn nicht. `halbiere(1)` sollte z.B. `(0, false)` zurückgeben, `halbiere(2)` dagegen `(1, true)`.
- Schreibe eine Funktion mit einem **variadischen** Parameter, die aus einer Liste von Zahlen die größte findet und als Ergebniswert liefert.
- Nutze `generateEvenUints` als Ausgangspunkt um eine Funktion `generateOddUints` zu schreiben, die ungerade positive Ganzzahlen generiert.
- Die Fibonacci-Sequenz ist definiert als: `fib(0) = 0, fib(1) = 1, fib(n) = fib(n-1) + fib(n-2)`.<br/>Schreibe eine **rekursive** Funktion, welche die n-te Fibonacci-Zahl ermitteln kann, `fib(n)`.
- Was sind `defer`, `panic` und `recover`? Wie kann man eine Laufzeit-Panik handhaben?
