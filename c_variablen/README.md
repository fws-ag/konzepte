# 1. Variablen

<!-- TOC -->

- [1. Variablen](#1-variablen)
  - [1.1. Wie man eine Variable benennen sollte](#11-wie-man-eine-variable-benennen-sollte)
  - [1.2. Geltungsbereich (Scope)](#12-geltungsbereich-scope)
  - [1.3. Konstanten](#13-konstanten)
  - [Deklaration Mehrerer Variablen](#deklaration-mehrerer-variablen)
  - [Ein Exemplarisches Programm](#ein-exemplarisches-programm)
  - [Überprüf' Dein Wissen](#%C3%BCberpr%C3%BCf-dein-wissen)

Bisher haben wir nur Programme gesehen, die Literale verwenden (Zahlen, Zeichenketten, etc.), aber solche Programme sind nicht besonders nützlich. Um wirklich nützliche Programme zu erstellen, müssen wir zwei neue Konzepte lernen: *Variablen* und *Kontrollflussanweisungen*. In diesem Abschnitt werden die Variablen näher erläutert.

Eine Variable ist ein Speicherort mit einem bestimmten Typ und einem dazugehörigen Namen.<br/>
Laß uns das Programm, das wir in Abschnitt b geschrieben haben, so ändern, dass es eine Variable verwendet:

```go
package main

import "fmt"

func main() {
  var x string = "Hello World"
  fmt.Println(x)
}
```

Man beachte, dass das Zeichenkettenliteral aus dem Originalprogramm in diesem Programm noch vorkommt, aber anstatt es direkt an die ``Println``-Funktion zu senden, weisen wir es stattdessen einer Variablen zu. Variablen werden in Go erstellt, indem zuerst das Schlüsselwort ``var`` verwendet, der Variablenname (``x``), der Typ (``string``) und schließlich ein Wert für die Variable (``"Hello World"``) angegeben wird. Der letzte Schritt ist optional, so dass eine alternative Art, das Programm zu schreiben, so aussehen könnte:

```go
package main

import "fmt"

func main() {
  var x string
  x = "Hello World"
  fmt.Println(x)
}
```

Variablen in Go sind ähnlich wie Variablen in der Algebra, aber es gibt einige feine Unterschiede:

Erstens, beim Anblick des ``=``-Symbols neigen wir dazu, zu lesen, dass *"x gleich der Zeichenkette Hello World"* ist. Es ist im Grunde nichts falsch daran, unser Programm so zu interpretieren, aber es ist besser, wenn du es in etwa so liest: "``x`` steht für die Zeichenkette ``Hello World``" oder "``x`` wird der Zeichenkette ``Hello World`` zugewiesen". Diese Unterscheidung ist wichtig, da (wie der Name schon sagt) Variablen ihren Wert während der gesamten Lebensdauer eines Programms ändern können. Probiere Folgendes aus:

```go
 main

import "fmt"

func main() {
  var x string
  x = "first"
  fmt.Println(x)
  x = "second"
  fmt.Println(x)
}
```

Du kannst auch das hier tun:

```go
var x string
x = "first "
x = x + "second"
fmt.Println(x)
```

Dieses Programm würde Unsinn ergeben, wenn man es wie ein algebraisches Theorem lesen würde. Aber es ergibt dann Sinn, wenn du das Programm als eine Liste von Befehlen liest. Wenn wir ``x = x + "second"`` sehen, sollten wir es wie "**weise die Verkettung des Wertes der Variablen `x` mit dem Stringliteral `second` der Variablen `x` zu**" lesen. Die rechte Seite des ``=`` wird zuerst ausgeführt und das Ergebnis wird dann der linken Seite des ``=`` zugewiesen.

Die ``x = x + y``-Formulierung ist in der Programmierung so verbreitet, dass Go, und auch viele andere Programmiersprachen, eine spezielle Zuordnungsanweisung hat: ``+=``. Wir hätten ``x = x + "second"`` als ``x += "second"`` schreiben können und es hätte das Gleiche bewirkt. (Andere Operatoren als ``+`` können auf die gleiche Weise verwendet werden)

Ein weiterer Unterschied zwischen Go und Algebra besteht darin, dass man ein anderes Symbol für Gleichheit verwendet: ``==``. (Zwei Gleichheitszeichen nebeneinander) ``==``ist ein Operator wie ``+`` und gibt einen Boolean zurück. Ein Beispiel:

```go
var x string = "hello"
var y string = "world
fmt.Println(x == y)
```

Dieses Programm sollte ``false`` ausgeben, da ``hello`` nicht das gleiche ist wie ``world``. Andererseits wird:

```go
var x string = "hello"
var y string = "hello"
fmt.Println(x == y)
```

``true`` ergeben, da die zwei Zeichenketten identisch sind.

Eine solche Codeeinheit, die sich in einen Wert auflöst, wird *Ausdruck* oder *Expression* genannt.

Da das Erstellen einer neuen Variablen mit einem *Initialwert* so üblich ist, bietet Go auch eine kürzere Anweisung:

```go
x := "Hello World"
```

Achte auf das ``:`` vor dem ``=`` und dass kein Typ angegeben wurde. Der Typ ist nicht notwendig, da der Go-Compiler in der Lage ist, den Typ aus dem Literalwert abzuleiten, den du der Variablen zuweist. (Da du ein Zeichenkettenliteral zuweist, erhält ``x`` den Typ ``string``) Der Compiler kann auch bei der ``var``-Anweisung den Typ ableiten:

```go
var x = "Hello World"
```

Generell empfiehlt es sich, nach Möglichkeit diese kürzere Form zu verwenden (``:=``).

Die Eigenschaft des Compilers, Typen abzuleiten nennt man *Inferenz*.

## 1.1. Wie man eine Variable benennen sollte

Die korrekte Benennung von Variablen ist ein wichtiger Bestandteil der Softwareentwicklung. Namen müssen mit einem Buchstaben beginnen und können Buchstaben, Zahlen oder das Symbol ``_`` (Unterstrich) enthalten. Der Go-Compiler interessiert sich nicht dafür, wie du eine Variable benennst (solange die eben genannten Regeln beachtet werden), also ist der Name für deinen Nutzen (und den anderer, die deinen Code lesen und interpretieren müssen) bestimmt. Such dir Namen aus, die den **Zweck** der Variablen klar beschreiben. Nehmen wir an, wir hätten folgendes:

```go
x := "Kastania"
fmt.Println("My dog's name is", x)
```

In diesem Fall ist *x* kein besonders geeigneter Name. Ein besserer wäre z.B.:

```go
name := "Kastania"
fmt.Println("My dog's name is", name)
```

Oder sogar:

```go
dogsName := "Kastania"
fmt.Println("My dog's name is", dogsname)
```

In diesem letzten Fall verwenden wir eine spezielle Methode, um mehrere Wörter in einem Variablennamen darzustellen, die als **Lower Camel Case** bekannt ist (auch bekannt als Mixed Case, Bumpy Caps, Camel Back oder Hump Back). Der erste Buchstabe des ersten Wortes ist klein, der erste Buchstabe der nachfolgenden Wörter ist groß und alle anderen Buchstaben sind klein. (``einNameDerAusMehrerenWortenBestehtUndInLowerCamelCaseGeschriebenIst``)

Wichtig ist, dass du dich fürs Erste an die Regel hältst, dass der erste Buchstabe des Namens klein geschrieben ist. Dies ist nämlich relevant für die Go. Warum, und unter welchen Umständen du den Namen einer Variablen oder Funktion mit einem Großbuchstaben beginnst, sehen wir in einem späteren Abschnitt.

## 1.2. Geltungsbereich (Scope)

Um auf das Programm zurückzukommen, das wir am Anfang dieses Skripts gesehen haben:

```go
package main

import "fmt"

func main() {
  var x string = "Hello World"
  fmt.Println(x)
}
```

Eine andere Art, dieses Programm zu schreiben, wäre wie folgt:

```go
package main

import "fmt"

var x string = "Hello World"

func main() {
    fmt.Println(x)
}
```

Zu beachten ist, dass wir die Variable aus dem Bereich der ``main``-Funktion heraus verschoben haben. Das bedeutet, dass andere Funktionen auf diese Variable zugreifen können:

```go
var x string = "Hello World"

func main() {
  fmt.Println(x)
}

func f() {
  fmt.Println(x)
}
```

Die Funktion ``f`` hat nun Zugriff auf die Variable ``x``. Angenommen, wir hätten stattdessen dies geschrieben:

```go
func main() {
    var x string = "Hello World"
    fnt.Println(x)
}

func f() {
    fmt.Println(x)
}
```

Wenn du dieses Programm ausführst, solltest du eine Fehlermeldung bekommen:

```sh
./main.go:11:14: undefined: x
```

Der Compiler teilt dir mit, dass die Variable ``x`` innerhalb der ``f``-Funktion nicht existiert. Sie existiert nur innerhalb der ``main``-Funktion. Die Menge der Stellen, an denen du ``x`` verwenden darfst, wird als *Scope* der Variable bezeichnet. Gemäß der Sprachspezifikation (frei übersetzt): "**Go wird unter Verwendung von Blöcken lexikalisch eingegrenzt**". Im Grunde bedeutet dies, dass die Variable innerhalb der nächsten geschweiften Klammern ``{ }`` (ein **Block**) existiert, einschließlich aller verschachtelten geschweiften Klammern (**Blöcke**), aber nicht außerhalb davon. Der Geltungsbereich kann zunächst etwas verwirrend sein; je mehr Beispiele von Go-Code wir betrachten, desto klarer sollte das Konzept des Scope werden.

## 1.3. Konstanten

Go unterstützt auch *Konstanten*. Konstanten sind im Wesentlichen Variablen, deren Werte nicht mehr nachträglich geändert werden können. Sie werden auf die gleiche Weise erstellt, wie du Variablen erstellst, aber anstatt das Schlüsselwort ``var`` zu verwenden, wird das Schlüsselwort ``const`` verwendet:

```go
package main

import "fmt"

func main() {
  const x string = "Hello World"
  fmt.Println(x)
}
```

Dies allerdings:

```go
const x string = "Hello World"
x = "Some other string"
```

Führt zu einem **Compile-Time**-Fehler (=*während der Kompilierung*):

```plain
.\main.go:7: cannot assign to x
```

Konstanten sind eine gute Möglichkeit, gängige Werte in einem Programm wiederzuverwenden, ohne sie jedes Mal neu zu schreiben. Beispielsweise ist Pi im math-Package als Konstante deklariert:

```go
const Pi float64 = 3.14159265358979323846264338327950288419716939937510582097494459
```

Ein kleines Beispiel:

```go
package main

import (
	"fmt"
	"math"
)

// convertLYToKM konvertiert Lichtjahre zu Kilometer
func convertLYToKM(ly float64) float64 {
	conversionFactor := 9.4607305 * math.Pow(10, 12) // 9.4607305 * 10^12, bzw. 9.4607305e+12km

	return ly * conversionFactor
}

func main() {
	const lightspeed float64 = 299792 // km/s
	var distance float64 = 56000000   // km

	fmt.Println("Mit Lichtgeschwindigkeit würde es", distance/lightspeed,
		"Sekunden dauern, bis wir die Distanz von 56000000km zurückgelegt haben,\n bzw. am Mars angekommen sind.")

	distance = convertLYToKM(1400) // km, 1400 Lichtjahre zu km

	fmt.Println("Und", distance/lightspeed,
		"Sekunden -also 1400 Jahre-, bis wir eine Distanz von 1400 Lichtjahren hinter uns haben.")
}
```

Der Output dieses Programms ist wie folgt:

```plain
$ go run main.go
Mit Lichtgeschwindigkeit würde es 186.7961786838875 Sekunden dauern, bis wir die Distanz von 56000000km zurückgelegt haben, bzw. am Mars angekommen sind.
Und 4.418070762395261e+10 Sekunden -also 1400 Jahre-, bis wir eine Distanz von 1400 Lichtjahren hinter uns haben.
```

## Deklaration Mehrerer Variablen

Go hat auch noch zwei weitere Kurzformen, für den Fall, dass du mehrere Variablen definieren möchtest:

1:

```go
var (
  a = 1
  b = 1
  c = 2
)
```

Verwende einfach das Schlüsselwort ``var`` (oder ``const``) gefolgt von Klammern, mit jeder Variablen in einer eigenen Zeile.

2:

```go
a, b, c := 1, 1 , 2
```

Schreibe die Namen der Variablen hintereinander und mit Kommata getrennt auf die linke Seite des ``:=`` Operators, und die Werte, die den Namen zugewiesen werden sollen in der Reihenfolge der Namen denen sie zugewiesen werden sollen auf die rechte Seite.

## Ein Exemplarisches Programm

Hier ist ein Beispielprogramm, das eine vom Benutzer eingegebene Zahl aufnimmt und verdoppelt:

```go
package main

import "fmt"

func main() {
  fmt.Print("Enter a number: ")
  var input float64
  fmt.Scanf("%f", &input)

  output := input * 2

  fmt.Println(output)
}
```

Wir verwenden eine weitere Funktion aus dem `fmt`-Paket, um die Benutzereingaben zu lesen (``Scanf``).</br>
``&input`` wird in einem späteren Abschnitt (*Pointer*) erläutert, denn jetzt müssen wir nur wissen, dass ``Scanf`` die Variable ``input`` mit der von uns eingegebenen Zahl füllt.

## Überprüf' Dein Wissen

- Welche Möglichkeiten gibt es, eine neue Variable zu erstellen?
- Was ist der Wert von ``x`` nach der Ausführung des folgenden Codes:

  ```go
  x := 100
  x += 5
  ```

- Was ist **Scope** und wie wird der Scope einer Variablen in Go bestimmt?
- Worin besteht der Unterschied zwischen ``var`` und ``const``?
- Schreibe, ausgehend vom Beispielprogramm,  ein Programm, das von Fahrenheit in Celsius umrechnet. Hinweis: **C = (F - 32) * 5/9**
- Entwickle ein zweites Programm, das von Fuß in Meter umwandelt. Hinweis: **1 ft = 0,3048 m**