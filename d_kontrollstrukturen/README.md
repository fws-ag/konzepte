# 1. Kontrollstrukturen

<!-- TOC -->
- [1. Kontrollstrukturen](#1-kontrollstrukturen)
  - [1.1. Grundlegende Begriffe](#11-grundlegende-begriffe)
    - [1.1.1. Ein Beispiel](#111-ein-beispiel)
  - [1.2. Warum wir Kontrollstrukturen brauchen](#12-warum-wir-kontrollstrukturen-brauchen)
  - [1.3. Das For-Statement](#13-das-for-statement)
  - [1.4. If](#14-if)
  - [1.5. Switch](#15-switch)
  - [1.6. Überprüf' Dein Wissen](#16-%C3%BCberpr%C3%BCf-dein-wissen)

In Abschnitt **a** haben wir gelernt, dass ein Go Programm von links nach rechts und von oben nach unten gelesen wird, wie ein Buch.
Auch die Ausführung unseres Programms geschieht, mit Ausnahme einiger Sonderfälle, in dieser Reihenfolge, d.h., dass Anweisungen die **über** oder **vor** anderen Anweisungen stehen auch zuerst ausgeführt werden:

```go
// ...
func main() {
    fmt.Println("Wird als erstes ausgeführt")
    fmt.Println("Wird als zweites ausgeführt")

    expressionen := (1 + 2) * (9 / 3) // 1 + 2 wird evaluiert, dann 9 / 3, dann 3 * 3

    fmt.Println(expressionen)
}
```

Die Sequenz, in der Anweisungen in einem *imperativen* Programm ausgeführt und Ausdrücke evaluiert werden, nennt man **Control Flow**.

Wir können den Control Flow mittels **Kontrollstrukturen** lenken, d.h., die Reihenfolge in der Anweisungen in unserem Programm ausgeführt werden beeinflussen.

Diese Tätigkeit nennt man dann **Flow Control** oder **Ablaufsteuerung**.

Eine Kontrollstruktur ist eine Codeeinheit die Variablen analysiert und eine Richtung wählt, in die das Programm basierend auf gegebenen Parametern gehen soll. Der Begriff Ablaufsteuerung beschreibt die Richtung, in die das Programm geht (in welche Richtung der Ablauf "fließt"). Daher ist es der grundlegende Entscheidungsprozess in einem Programm; die Ablaufsteuerung bestimmt, wie ein Programm reagieren wird, wenn bestimmte Bedingungen und Parameter gegeben sind.

## 1.1. Grundlegende Begriffe

Diese Ausgangsbedingungen und Parameter werden als **Präkondition** bezeichnet. Die Präkondition ist der Zustand von Variablen vor der Verwendung einer Kontrollstruktur. Basierend auf diesen Präkonditionen führt das Programm einen Algorithmus (die Kontrollstruktur) aus, um festzustellen, was in diesem Fall zu tun ist. Das Ergebnis wird als Postkondition bezeichnet. Die Postkondition ist der Zustand der Variablen nach dem Ausführen des Algorithmus.

>Zusammengefasst:
> - Präkondition/Preconditon: Der Zustand von Variablen bevor diese durch eine Kontrollstrukture beeinflusst werden.
> - Algorithmus: Eine Abfolge von Schritten, die unter Berücksichtigung der Präkonditionen ausgeführt werden.
> - Post-Kondition/Postcondition: Der Zustand von Variablen nachdem diese durch eine Kontrollstruktur beeinflusst wurden -> Das Ergebnis des Algorithmus.

Wir müssen uns auch die folgenden Begriffe in Erinnerung rufen:

> - **Statement**: Eine syntaktische Einheit einer imperativen* Programmiersprache, die eine auszuführende Aktion angibt, z.B. `fmt.Println("Print this!")`.
> - **Expression**: Jede gültige Codeeinheit, die in einen Wert aufgelöst wird, etwa `1 + 1`.
>   - Eine besondere Form des Ausdrucks ist der **Konditionsausdruck**, der spezifisch einen Wahrheitswert (true oder false) ergibt.<br/>
Ein Beispiel wäre `1 == 1`, was `true` ergibt.
> - **Deklaration**: Bindung eines Namens (Kennung/Identifier) an eine Konstante, Variable, Funktion, Label oder Package.
>   - Jede verwendete Kennung in einem Programm muss in dem Block deklariert worden sein, in dem sie referenziert wird.
>   - Eine Kennung darf in einem Block nicht doppelt deklariert werden.

### 1.1.1. Ein Beispiel

Am Beispiel des Verkehrs können wir das Konzept einer Kontrollstruktur verbildlichen:

Ansatz:
Precondition: Das Fahrzeug ist in Bewegung | Die Ampel ist rot.<br/>
Algorithmus:<br/>
\# Ist die Ampel grün? Wenn ja, so darf das Fahrzeug sich ungehindert weiter bewegen.<br/>
\# Ist die Ampel rot? Wenn ja, so muss das Fahrzeug stoppen.<br/>

Postcondition: Das Fahrzeug kam zum Stehen, weil die Ampel rot war.

## 1.2. Warum wir Kontrollstrukturen brauchen

Jetzt, da wir wissen, wie man Variablen verwendet, ist es an der Zeit, mit dem Schreiben einiger sinnvoller Programme zu beginnen. Zuerst schreiben wir ein Programm, das bis 10 zählt, beginnend bei 1, mit jeder Zahl in einer eigenen Zeile. Anhand dessen, was wir bisher gelernt haben, könnten wir das hier schreiben:

```go
package main

import "fmt"

func main() {
  fmt.Println(1)
  fmt.Println(2)
  fmt.Println(3)
  fmt.Println(4)
  fmt.Println(5)
  fmt.Println(6)
  fmt.Println(7)
  fmt.Println(8)
  fmt.Println(9)
  fmt.Println(10)
}
```

Oder, leicht abgeändert, dies:

```go
package main
import "fmt"

func main() {
  fmt.Println(`1
2
3
4
5
6
7
8
9
10`)
}
```

Aber beide dieser Programme sind ziemlich mühsam zu schreiben. Was wir brauchen, ist eine Möglichkeit, Etwas mehrmals zu tun.

## 1.3. Das For-Statement

Die for-Anweisung ermöglicht es uns, eine Reihe von Anweisungen (jeweils ein Block) wiederholt auszuführen. Es ist also eine Schleife/Loop. So sieht es aus, wenn wir unser vorheriges Programm unter Verwendung einer for-Anweisung neu schreiben:

```go
package main

import "fmt"

func main() {
	countTo := 10

	for i := 1; i <= countTo; i++ {
		fmt.Println(i)
    }

    fmt.Println("Fertig gezählt!")
}
```

Okay, es gibt einiges, was wir hier entschlüsseln müssen!

Gehen wir den Inhalt der `main`-Funktion Zeilenweise durch:

`countTo := 10` - Wir deklarieren eine Variable mit dem Literalwert 10.

`for i := 1; i <= countTo; i++ {` - Das ist schon etwas komplexer. Hier die detailierte Erklärung:

Die Basis für ein for-Statement bilden die folgenden drei Komponenten:

>- Das *init*-Statement. Es wird einmalig vor der ersten Wiederholung ausgeführt.
>   - In unserem Fall ist `ì := 1` das init-Statement.
>   - Die init-Anweisung ist oft eine kurze Variablendeklaration, wobei die dort deklarierten Variablen nur im Scope des for-Statements sichtbar sind.
>- Der Konditionsausdruck, der vor jeder Wiederholung evaluiert wird. Solange der Konditionsausdruck `true` ergibt wird der Algorithmus des for-Statements ausgeführt, sobald er `false` ergibt wird die Schleife unterbrochen, und die nächste Zeile des Programms, die dem for-Statement folgt, wird ausgeführt.
>   - In unserem Fall ist `ì <= countTo` der Konditionsausdruck.
>- Das post-Statement, das nach jeder Wiederholung ausgeführt wird.
>   - In unserem Fall ist `i++` das post-Statement. `i++` ist ein Synonym für `i = i + 1` bzw. `i += 1`, `i` wird also inkrementiert, d.h., schrittweise um 1 erhöht.

Diese drei Komponenten werden durch ein Semikolon `;` getrennt. Wir benötigen nicht immer alle drei Komponenten, aber dazu später mehr...

Die nächste Zeile, `fmt.Println(i)`, stelllt den Algorithmus dieser Kontrollstruktur dar.
Es ist der Code, den wir wiederholt ausführen. Während jeder Wiederholung geben wir den Wert von `i` ans Terminal aus. Der Wert von `i` wird nach jeder Wiederholung inkrementiert, bis er größer als countTo ist. Danach wird die Schleife unterbrochen, weil der Konditionsausdruck `i <= countTo` -"i ist kleiner oder gleich wie `countTo`"-  `false` ergibt. Nachdem die Schleife unterbrochen wurde wird die nächste Zeile des Programms, `fmt.Println("Fertig gezählt!")` ausgeführt.

Go's for-Statements sind flexibel. Wir können den Code, den wir oben geschrieben haben in mehreren Arten umstellen, z.B.:

```go
package main

import "fmt"

func main() {
	i := 1

	for i <= 10 {
		fmt.Println(i)
		i++
	}
}
```

Wie ich oben schon angedeutet habe, müssen wir nicht alle drei Komponenten eines for-Statements verwenden.
Hier benutzen wir nur den Konditionsausdruck, um die Lebenszeit der Schleife zu begrenzen. Endlos-Schleifen sehen wir uns gleich an!<br/>
Die Funktion der beiden anderen Komponenten haben wir nun aus dem for-Statement rausverlagert.
Wir deklarieren `i := 1` im Funktionskörper von `main`, und verlagern die Funktionalität des post-Statements in den Körper des for-Statements, es gehört nun zum Algorithmus und wird bei jeder Wiederholung ausgeführt, genau als stünde es es post-Statement oben drüber.

Um den Ablauf des Programms zu verdeutlichen, stell' es dir als Liste von Schritten vor:

> - Erstelle eine Variable mit dem Namen `i` und dem Wert `1`
> - Überprüfe, ob `i <= 10`. Ja!
>   - Printe `i`. (1)
>   - Weise `i` den Wert `i + 1` zu. (`i` ist jetzt 2)
> - Überprüfe, ob `i <= 10`. Ja!
>   - Printe `i`. (2)
>   - Weise `i` den Wert `i + 1` zu. (`i` ist jetzt 3)
> - [...]
> - Überprüfe, ob `i <= 10`. Ja!
>   - Printe `i`. (10)
>   - Weise `i` den Wert `i + 1` zu. (`i` ist jetzt **11**)
> - Überprüfe, ob `i <= 10`. **Nein**! (`i` ist 11)
> - Nichts mehr zu tun, unterbreche die Schleife

Andere Programmiersprachen haben viele verschiedene Arten von Schleifen (*while, do, until, foreach,*...), aber Go hat nur eine, die auf verschiedene Weise verwendet werden kann. Wie du gesehen hast, können wir das for-Statement auf verschiedene Art und Weise verwenden, weshalb es -basierend auf der Art, in der es geschrieben wird- die Eigenschaften aller dieser verschiedenen Arten von Schleifen annehmen kann.

Wir werden in späteren Abschnitten weitere Möglichkeiten behandeln, die for-Schleife zu verwenden.

## 1.4. If

Ändern wir das Programm, das wir gerade geschrieben haben, so, dass es nicht bloss die Zahlen 1-10 auf jede Zeile druckt, sondern auch angibt, ob die Zahl gerade oder ungerade ist, so dass der Output wie folgt aussieht:

```plain
1 ungerade
2 gerade
3 ungerade
4 gerade
5 ungerade
6 gerade
7 ungerade
8 gerade
9 ungerade
10 gerade
```

Zuerst brauchen wir eine Methode, um festzustellen, ob eine Zahl gerade oder ungerade ist. Eine einfache Art dies festzustellen ist, die Zahl durch 2 zu teilen und den Rest der Division zu betrachen: Wenn nichts mehr übrig ist, dann ist die Zahl gerade, sonst ist sie ungerade. Wie finden wir also den Rest nach der Division in Go? Wir verwenden den `%`-Operator. `1 % 2` ist gleich 1, `2 % 2` ist gleich 0, `3 % 2` ist gleich 1 und so weiter.

Als nächstes brauchen wir eine Möglichkeit, unter Berücksichtigun einer Kondition eine Entscheidung darüber zu trefen, eine von mehreren möglichen Sachen zu tun. Dazu verwenden wir die `if`-Anweisung:

```go
if i % 2 == 0 {
  // gerade
} else {
  // ungerade
}
```

Ein `if`-Statements ist insofern ähnlich wie ein `for`-Statement, als dass es einen Konditionsausdruck mit anschließendem Block aufweist. `if`-Anweisungen haben auch einen optionalen `else`-Anteil. Wenn die Bedingung als `true` ausgewertet wird, dann wird der Block nach der Bedingung ausgeführt, andernfalls wird entweder der Block übersprungen oder es wird der `else`-Block ausgeführt, falls vorhanden.

`if`-Statements können zudem auch `else if` Komponenten enthalten:

```go
if i % 2 == 0 {
  // durch 2 teilbar
} else if i % 3 == 0 {
  // durch 3 teilbar
} else if i % 4 == 0 {
  // durch 4 teilbar
}
```

Die Konditionsausdrücke werden von oben nach unten ausgewertet und bei der ersten Bedingung, die zu `true` führt, wird der zugehörige Block ausgeführt. Keiner der anderen Blöcke wird ausgeführt, auch wenn ihre Konditionen ebenfalls erfüllt sind.

Zusammenfassend ergibt sich daraus Folgendes:

```go
package main

import "fmt"

func main() {
  for i := 1; i <= 10; i++ {
    if i % 2 == 0 {
      fmt.Println(i, "gerade")
    } else {
      fmt.Println(i, "ungerade")
    }
  }
}
```

Betrachten wir das Programm wieder schrittweise:

> - Deklariere eine Variable mit dem Namen `i` vom Typ `int` und weise ihr den Wert `1` zu.
> - Überprüfe, ob `i <= 10` -> Ja!
>   - Wenn ja: Führe den Block des for-Statements aus.
>       - Überprüfe, ob der Rest von `i`÷`2` `0` ist -> Nein!
>       - -> Führe den `else`-Block aus.
>   - Printe `i` gefolgt von `ungerade`.
>   - Inkrementiere `i` (**post-Statement**!)
> - Überprüfe, ob `i <= 10` -> Ja!
>   - Wenn ja: Führe den Block des for-Statements aus.
>       - Überprüfe, ob der Rest von `i`÷`2` `0` ist -> Ja!
>       - -> Führe den mit der Kondition verknüpften Block aus.
>       - Printe `i` gefolgt von `gerade`.
> - [...]

Der `%`-Operator, auch *Modulo* oder *Division-mit-Rest* genannt, erweist sich beim Programmieren als ungemein nützlich. Du wirst feststellen, dass er so ziemlich überall auftaucht, von Tabellen, die im Zebrastreifen-Look dargestellt werden sollen, bis hin zur Partitionierung von Datensätzen.

## 1.5. Switch

Angenommen, wir möchten ein Programm schreiben, das die deutschen Namen für Zahlen ausgibt. Nach dem, was wir bisher gelernt haben, könnten womöglich mit diesem Ansatz beginnen:

```go
if i == 0 {
  fmt.Println("Null")
} else if i == 1 {
  fmt.Println("Eins")
} else if i == 2 {
  fmt.Println("Zwei")
} else if i == 3 {
  fmt.Println("Drei")
} else if i == 4 {
  fmt.Println("Vier")
} else if i == 5 {
  fmt.Println("Fünf")
}
```

Da es ziemlich mühsam wäre, ein Programm auf diese Weise zu schreiben, bietet Go eine weitere Anweisung, um dies zu erleichtern: das switch-Statement. Das Programm könnte man z.B. so umschreiben:

```go
switch i {
case 0: fmt.Println("Null")
case 1: fmt.Println("Eins")
case 2: fmt.Println("Zwei")
case 3: fmt.Println("Drei")
case 4: fmt.Println("Vier")
case 5: fmt.Println("Fünf")
default: fmt.Println("Unbekannte Zahl")
}
```

Ein switch-Statement beginnt mit dem Schlüsselwort `switch`, gefolgt von einer Expression (in diesem Fall `i`) und dann einer Reihe von Fällen. Der Wert der Expression wird mit der Expression nach jedem `case` Keyword verglichen. Wenn sie identisch sind, wird die Anweisung(en) nach dem `:` ausgeführt.

Wie bei einer `if`-Anweisung wird jeder Fall von oben nach unten geprüft und der erste, der eintritt, wird ausgewählt. Ein `switch` unterstützt auch einen **Standardfall**, der eintritt, wenn keiner der Fälle mit dem Wert übereinstimmt. (Wie das `else` in einer `if`-Anweisung)

Wir können die Expression nach dem `switch` Keyword auch weglassen. In diesem Fall müssen wir für jeden Fall einen vollständigen Konditionsausdruck schreiben:

```go
switch {
case i == 0: fmt.Println("Null")
case i == 1: fmt.Println("Eins")
case i == 2: fmt.Println("Zwei")
case i == 3: fmt.Println("Drei")
case i == 4: fmt.Println("Vier")
case i == 5: fmt.Println("Fünf")
default: fmt.Println("Unbekannte Zahl")
}
```

Dies sind die wesentlichen Kontrollflussanweisungen. Zusätzliche Anweisungen werden in späteren Abschnitten erläutert.

## 1.6. Überprüf' Dein Wissen

- Was gibt das folgende Programm ans Terminal aus?</br>

    ```go
    i := 10
    if i > 10 {
      fmt.Println("Big")
    } else {
      fmt.Println("Small")
    }
    ```

- Schreibe ein Programm, das alle Zahlen zwischen 1 und 100 ausgibt, die ohne Rest durch 3 teilbar sind (3, 6, 9 usw.)
- Schreibe ein Programm, das die Zahlen von 1 bis 100 ausgibt. Es gelten die folgenden Sonderfälle:
  - Für Zahlen, die ein Vielfaches von 3 sind, gebe anstelle der Zahl die Zeichenkette "Fizz" aus.
  - Für Zahlen, die ein Vielfaches von 5 sind gebe anstelle der Zahl die Zeichenkette "Buzz" aus.
  - Für Zahlen, die ein Vielfaches von 3 UND 5 sind, gebe anstelle der Zahl die Zeichenkette "FizzBuzz" aus.