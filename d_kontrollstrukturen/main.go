package main

import "fmt"

func main() {
	countTo := 10

	for i := 1; i <= countTo; i++ {
		if i%2 == 0 {
			fmt.Println(i, "Gerade")
		} else {
			fmt.Println(i, "Ungerade")
		}
	}

	fmt.Println("Fertig gezählt!")
}
