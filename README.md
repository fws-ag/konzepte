# 1. Konzepte

<!-- TOC -->

- [1. Konzepte](#1-konzepte)
  - [1.1. Formatierung](#11-formatierung)
  - [1.2. Skript](#12-skript)
  - [1.3. Quellcode](#13-quellcode)

Diese Repository enthält Erklärungen und Beispiele zu allen essentiellen Konzepten,
die wir in der AG, speziell im Teil zu den Grundlagen des Programmierens, behandeln.

Zu jedem Konzept gibt es einen demnach benannten Ordner.
In diesen Ordner findest du ein Skript und Quellcode mit Beispielen.

## 1.1. Formatierung

Es gibt einige Konventionen, die dir mit dem Verständniss der Skripte helfen sollen:

- Neue Begriffe werden in der Regel **bold** dargestellt
- Code wird in zwei Arten dargestellt:
  - Kurze Auschnitte so: ``var x = 1``
  - Längere Auschnitte, die mehrere Zeilen umfassen, so:

    ```go
    package main

    import "fmt"

    func main() {
        fmt.Println("Hello World")
    }
    ```

  - Befehle, die du im Terminal ausführen sollst, sind ebenfalls wie Code formatiert, allerding ist allen Zeilen die auszuführen sind ein **Dollarzeichen** vorausgestellt.<br/>
  Zum Beispiel ``$ go run main.go``. Das Dollarzeichen gehört **nicht** zum Befehl!<br/>
  Wenn mehrere Befehle dargestellt werden, die nacheinander ausgeführt werden und eventuell Output produzieren, könnte das etwa so aussehen:

  ```plain
  $ mkdir hello
  $ cd hello/
  $ touch main.go
  $ ls
  main.go <- *ANMERKUNG* Diese Zeile ist der Output des ls Befehls
  ```

## 1.2. Skript

Das Skript ist zum lesen gedacht...
Es sollte online -*direkt hier auf Gitlab*-
gelesen werden, da es in besonderer Weise formatiert ist.

Das Skript sollte -genau wie dieser Text- automatisch im Browser angezeigt werden, wenn ihr in
den entsprechenden Ordner geht!

## 1.3. Quellcode

Der Quellcode hingegen dient zum Verständnis des Skripts, und ist zum experimentieren
gedacht! Am Ende des Skripts findet ihr auch Übungen, die oftmals eine Veränderung
oder Ergänzung am Quellcode fordern.

Der Quellcode kann in der Regel direkt ausgeführt werden, mittels

``$ go run _dateiname.go_``
